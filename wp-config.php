<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'fundisa' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'mysql' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '4(#BO&8(K0/Z9,xZ(9m]l(TX+XCKQDCvRRh6ZOV0y4igd].&`y4/gR#N1^U*j?#I' );
define( 'SECURE_AUTH_KEY',  ']Z(wiAY_}vMR]?ux@@F07pkTKlf5Xx<W#x0M`,EV$, YqWtwB#2=mxvGPRBdS1P0' );
define( 'LOGGED_IN_KEY',    'g1yO}r0zc?:7m&P~]O+h;R#5<8I#o)t@U7!/@#T2 !8``Mt=l9Woc0/+3+w;4L9L' );
define( 'NONCE_KEY',        '/nfsb{^L{ETe,,|M!6]+Ya<&@|f.s;}A6=M4(B=E`rl:<waRvXun/A5+U$dH#fyP' );
define( 'AUTH_SALT',        'fpqOnOd3/sbz|sTGR|dWNZcT5ey;{q$OSY.u<8Qx&vT./p4M w_>d1y!]Q_or@<6' );
define( 'SECURE_AUTH_SALT', 'tV8,co#.bs<KI=x1_oOu#_fdBAKo%-kbeV+8a$zaI-qDeP5P(4h$olAZu|k]*2i-' );
define( 'LOGGED_IN_SALT',   'bc$OCEgA1w^_W5BK:{pVs0/W bf&K$l 3g(8=7{bS`LDLmD0xHCNYZD<[W>Z6I{4' );
define( 'NONCE_SALT',       'eHzQFDbC+fvwfo*K-8R*Pein2QV-CN*~9RoE7E)&lrRE<3ngvI7hT&ai?C(n?~Tx' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
