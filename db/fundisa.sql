-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 27, 2019 at 02:37 PM
-- Server version: 5.6.37
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fundisa`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE IF NOT EXISTS `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE IF NOT EXISTS `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-09-17 06:10:00', '2019-09-17 06:10:00', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE IF NOT EXISTS `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE IF NOT EXISTS `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB AUTO_INCREMENT=284 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/profile.local', 'yes'),
(2, 'home', 'http://localhost/profile.local', 'yes'),
(3, 'blogname', 'Fundisa', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'sbuja.gbza@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:5:{i:0;s:23:"grid-plus/grid-plus.php";i:1;s:23:"ml-slider/ml-slider.php";i:2;s:59:"ultimate-social-media-icons/ultimate_social_media_icons.php";i:3;s:24:"wordpress-seo/wp-seo.php";i:4;s:24:"wpforms-lite/wpforms.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'shark-business', 'yes'),
(41, 'stylesheet', 'shark-business', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'author', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:59:"ultimate-social-media-icons/ultimate_social_media_icons.php";s:20:"sfsi_Unistall_plugin";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '71', 'yes'),
(84, 'page_on_front', '12', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'wp_user_roles', 'a:7:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:62:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:20:"wpseo_manage_options";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:35:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:15:"wpseo_bulk_edit";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:13:"wpseo_manager";a:2:{s:4:"name";s:11:"SEO Manager";s:12:"capabilities";a:37:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:15:"wpseo_bulk_edit";b:1;s:28:"wpseo_edit_advanced_metadata";b:1;s:20:"wpseo_manage_options";b:1;}}s:12:"wpseo_editor";a:2:{s:4:"name";s:10:"SEO Editor";s:12:"capabilities";a:36:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:15:"wpseo_bulk_edit";b:1;s:28:"wpseo_edit_advanced_metadata";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:8:{s:19:"wp_inactive_widgets";a:6:{i:0;s:10:"archives-2";i:1;s:6:"meta-2";i:2;s:8:"search-2";i:3;s:12:"categories-2";i:4;s:14:"recent-posts-2";i:5;s:17:"recent-comments-2";}s:9:"sidebar-1";a:0:{}s:14:"home-page-area";a:1:{i:0;s:19:"metaslider_widget-2";}s:8:"footer-1";a:0:{}s:8:"footer-2";a:0:{}s:8:"footer-3";a:0:{}s:8:"footer-4";a:0:{}s:13:"array_version";i:3;}', 'yes'),
(102, 'cron', 'a:7:{i:1569597001;a:1:{s:34:"wp_privacy_delete_old_export_files";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1569607801;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1569651000;a:1:{s:32:"recovery_mode_clean_expired_keys";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1569651025;a:2:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1569651027;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1569670016;a:1:{s:19:"wpseo-reindex-links";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(113, 'recovery_keys', 'a:0:{}', 'yes'),
(114, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:58:"http://downloads.wordpress.org/release/wordpress-5.2.3.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:58:"http://downloads.wordpress.org/release/wordpress-5.2.3.zip";s:10:"no_content";s:69:"http://downloads.wordpress.org/release/wordpress-5.2.3-no-content.zip";s:11:"new_bundled";s:70:"http://downloads.wordpress.org/release/wordpress-5.2.3-new-bundled.zip";s:7:"partial";s:68:"http://downloads.wordpress.org/release/wordpress-5.2.3-partial-2.zip";s:8:"rollback";b:0;}s:7:"current";s:5:"5.2.3";s:7:"version";s:5:"5.2.3";s:11:"php_version";s:6:"5.6.20";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"5.0";s:15:"partial_version";s:5:"5.2.2";}}s:12:"last_checked";i:1569564962;s:15:"version_checked";s:5:"5.2.2";s:12:"translations";a:0:{}}', 'no'),
(115, 'theme_mods_twentynineteen', 'a:6:{s:18:"custom_css_post_id";i:-1;s:18:"nav_menu_locations";a:2:{s:6:"menu-1";i:2;s:6:"footer";i:2;}s:13:"primary_color";s:6:"custom";s:17:"primary_color_hue";i:206;s:12:"image_filter";i:0;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1569243280;s:4:"data";a:2:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:0:{}}}}', 'yes'),
(120, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1569564967;s:7:"checked";a:4:{s:14:"shark-business";s:5:"1.1.4";s:14:"twentynineteen";s:3:"1.4";s:15:"twentyseventeen";s:3:"2.2";s:13:"twentysixteen";s:3:"2.0";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'no'),
(122, 'can_compress_scripts', '1', 'no'),
(139, 'recently_activated', 'a:0:{}', 'yes'),
(142, 'grid_plus', 'a:1:{s:13:"5d8897941e32d";a:3:{s:2:"id";s:13:"5d8897941e32d";s:4:"name";s:8:"New Grid";s:4:"type";s:4:"grid";}}', 'yes'),
(143, 'grid_plus_5d8897941e32d', 'a:5:{s:2:"id";s:13:"5d8897941e32d";s:4:"name";s:8:"New Grid";s:11:"grid_config";a:55:{s:2:"id";s:13:"5d8897941e32d";s:4:"name";s:8:"New Grid";s:6:"height";s:3:"390";s:12:"height_ratio";s:1:"2";s:11:"width_ratio";s:1:"4";s:4:"type";s:4:"grid";s:7:"columns";s:1:"4";s:6:"gutter";s:2:"10";s:13:"item_per_page";s:1:"4";s:10:"total_item";s:0:"";s:15:"fix_item_height";s:5:"false";s:10:"crop_image";s:5:"false";s:12:"disable_link";s:5:"false";s:21:"custom_content_enable";s:5:"false";s:19:"custom_content_numb";s:1:"0";s:4:"loop";s:5:"false";s:6:"center";s:5:"false";s:9:"main_skin";s:25:"thumbnail-title-hover-top";s:12:"carousel_rtl";s:5:"false";s:21:"carousel_height_ratio";s:1:"2";s:20:"carousel_width_ratio";s:1:"3";s:18:"carousel_next_text";s:43:"<i class=&quot;fa fa-angle-right&quot;></i>";s:18:"carousel_prev_text";s:42:"<i class=&quot;fa fa-angle-left&quot;></i>";s:26:"carousel_desktop_large_col";s:1:"6";s:28:"carousel_desktop_large_width";s:4:"1200";s:27:"carousel_desktop_medium_col";s:1:"5";s:29:"carousel_desktop_medium_width";s:3:"992";s:26:"carousel_desktop_small_col";s:1:"4";s:28:"carousel_desktop_small_width";s:3:"768";s:19:"carousel_tablet_col";s:1:"3";s:21:"carousel_tablet_width";s:3:"600";s:25:"carousel_tablet_small_col";s:1:"2";s:27:"carousel_tablet_small_width";s:3:"480";s:19:"carousel_mobile_col";s:1:"1";s:21:"carousel_mobile_width";s:1:"1";s:8:"autoplay";s:5:"false";s:20:"autoplay_hover_pause";s:5:"false";s:13:"autoplay_time";s:4:"3000";s:8:"show_dot";s:5:"false";s:8:"show_nav";s:5:"false";s:14:"animation_type";s:6:"zoomIn";s:15:"pagination_type";s:9:"load-more";s:14:"page_next_text";s:4:"Next";s:14:"page_prev_text";s:8:"Previous";s:18:"page_loadmore_text";s:9:"Load more";s:14:"category_color";s:0:"";s:20:"category_hover_color";s:0:"";s:25:"no_image_background_color";s:0:"";s:16:"background_color";s:0:"";s:10:"icon_color";s:0:"";s:16:"icon_hover_color";s:0:"";s:11:"title_color";s:0:"";s:17:"title_hover_color";s:0:"";s:13:"excerpt_color";s:0:"";s:10:"custom_css";s:0:"";}s:16:"grid_data_source";a:11:{s:9:"post_type";s:4:"post";s:12:"grid_gallery";s:0:"";s:10:"categories";s:0:"";s:13:"show_category";s:4:"none";s:15:"cate_multi_line";s:5:"false";s:7:"authors";s:0:"";s:11:"include_ids";s:0:"";s:11:"exclude_ids";s:0:"";s:15:"attachment_type";s:11:"choose_item";s:5:"order";s:3:"ASC";s:8:"order_by";s:0:"";}s:11:"grid_layout";a:4:{i:0;a:8:{s:1:"x";s:1:"0";s:1:"y";s:1:"0";s:5:"width";s:1:"1";s:6:"height";s:2:"20";s:4:"skin";s:25:"thumbnail-title-hover-top";s:8:"template";s:97:"C:\\\\Ampps\\\\www\\\\profile.local\\\\wp-content\\\\plugins\\\\grid-plus/skins/thumbnail-title-hover-top.php";s:16:"item_width_ratio";s:1:"1";s:17:"item_height_ratio";s:1:"1";}i:1;a:8:{s:1:"x";s:1:"1";s:1:"y";s:1:"0";s:5:"width";s:1:"1";s:6:"height";s:2:"20";s:4:"skin";s:25:"thumbnail-title-hover-top";s:8:"template";s:97:"C:\\\\Ampps\\\\www\\\\profile.local\\\\wp-content\\\\plugins\\\\grid-plus/skins/thumbnail-title-hover-top.php";s:16:"item_width_ratio";s:1:"1";s:17:"item_height_ratio";s:1:"1";}i:2;a:8:{s:1:"x";s:1:"2";s:1:"y";s:1:"0";s:5:"width";s:1:"1";s:6:"height";s:2:"20";s:4:"skin";s:25:"thumbnail-title-hover-top";s:8:"template";s:97:"C:\\\\Ampps\\\\www\\\\profile.local\\\\wp-content\\\\plugins\\\\grid-plus/skins/thumbnail-title-hover-top.php";s:16:"item_width_ratio";s:1:"1";s:17:"item_height_ratio";s:1:"1";}i:3;a:8:{s:1:"x";s:1:"3";s:1:"y";s:1:"0";s:5:"width";s:1:"1";s:6:"height";s:2:"20";s:4:"skin";s:25:"thumbnail-title-hover-top";s:8:"template";s:97:"C:\\\\Ampps\\\\www\\\\profile.local\\\\wp-content\\\\plugins\\\\grid-plus/skins/thumbnail-title-hover-top.php";s:16:"item_width_ratio";s:1:"1";s:17:"item_height_ratio";s:1:"1";}}}', 'no'),
(144, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(148, 'WPLANG', '', 'yes'),
(149, 'new_admin_email', 'sbuja.gbza@gmail.com', 'yes'),
(154, 'wpforms_version', '1.5.3.1', 'yes'),
(155, 'wpforms_activated', 'a:1:{s:4:"lite";i:1569233579;}', 'yes'),
(158, 'widget_wpforms-widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(159, '_amn_wpforms-lite_to_check', '1570138339', 'yes'),
(160, 'wpforms_review', 'a:2:{s:4:"time";i:1569233580;s:9:"dismissed";b:0;}', 'yes'),
(163, 'widget_metaslider_widget', 'a:2:{i:2;a:2:{s:9:"slider_id";s:2:"23";s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(164, 'metaslider_tour_cancelled_on', 'create-slide', 'yes'),
(165, 'ms_hide_all_ads_until', '1570444967', 'yes'),
(166, 'metaslider_systemcheck', 'a:2:{s:16:"wordPressVersion";b:0;s:12:"imageLibrary";b:0;}', 'no'),
(167, 'ml-slider_children', 'a:0:{}', 'yes'),
(171, 'analyst_cache', 's:6:"a:0:{}";', 'yes'),
(173, 'show_new_notification', 'yes', 'yes'),
(174, 'show_premium_cumulative_count_notification', 'yes', 'yes'),
(175, 'sfsi_section1_options', 's:582:"a:16:{s:16:"sfsi_rss_display";s:3:"yes";s:18:"sfsi_email_display";s:3:"yes";s:21:"sfsi_facebook_display";s:3:"yes";s:20:"sfsi_twitter_display";s:3:"yes";s:19:"sfsi_google_display";s:2:"no";s:22:"sfsi_pinterest_display";s:2:"no";s:21:"sfsi_telegram_display";s:2:"no";s:15:"sfsi_vk_display";s:2:"no";s:15:"sfsi_ok_display";s:2:"no";s:19:"sfsi_wechat_display";s:2:"no";s:18:"sfsi_weibo_display";s:2:"no";s:22:"sfsi_instagram_display";s:2:"no";s:21:"sfsi_linkedin_display";s:2:"no";s:20:"sfsi_youtube_display";s:2:"no";s:19:"sfsi_custom_display";s:0:"";s:17:"sfsi_custom_files";s:0:"";}";', 'yes'),
(176, 'sfsi_curlErrorNotices', 'yes', 'yes'),
(177, 'sfsi_curlErrorMessage', '1', 'yes'),
(178, 'sfsi_section2_options', 's:1886:"a:48:{s:12:"sfsi_rss_url";s:36:"http://localhost/profile.local/feed/";s:14:"sfsi_rss_icons";s:5:"email";s:14:"sfsi_email_url";N;s:24:"sfsi_facebookPage_option";s:2:"no";s:21:"sfsi_facebookPage_url";s:0:"";s:24:"sfsi_facebookLike_option";s:3:"yes";s:25:"sfsi_facebookShare_option";s:3:"yes";s:21:"sfsi_twitter_followme";s:2:"no";s:27:"sfsi_twitter_followUserName";s:0:"";s:22:"sfsi_twitter_aboutPage";s:3:"yes";s:17:"sfsi_twitter_page";s:2:"no";s:20:"sfsi_twitter_pageURL";s:0:"";s:26:"sfsi_twitter_aboutPageText";s:82:"Hey, check out this cool site I found: www.yourname.com #Topic via@my_twitter_name";s:16:"sfsi_google_page";s:2:"no";s:19:"sfsi_google_pageURL";s:0:"";s:22:"sfsi_googleLike_option";s:3:"yes";s:23:"sfsi_googleShare_option";s:3:"yes";s:20:"sfsi_youtube_pageUrl";s:0:"";s:17:"sfsi_youtube_page";s:2:"no";s:24:"sfsi_youtubeusernameorid";s:0:"";s:17:"sfsi_ytube_chnlid";s:0:"";s:19:"sfsi_youtube_follow";s:2:"no";s:19:"sfsi_pinterest_page";s:2:"no";s:22:"sfsi_pinterest_pageUrl";s:0:"";s:23:"sfsi_pinterest_pingBlog";s:0:"";s:19:"sfsi_instagram_page";s:2:"no";s:22:"sfsi_instagram_pageUrl";s:0:"";s:18:"sfsi_linkedin_page";s:2:"no";s:21:"sfsi_linkedin_pageURL";s:0:"";s:20:"sfsi_linkedin_follow";s:2:"no";s:27:"sfsi_linkedin_followCompany";s:0:"";s:23:"sfsi_linkedin_SharePage";s:3:"yes";s:30:"sfsi_linkedin_recommendBusines";s:2:"no";s:30:"sfsi_linkedin_recommendCompany";s:0:"";s:32:"sfsi_linkedin_recommendProductId";s:0:"";s:21:"sfsi_CustomIcon_links";s:0:"";s:18:"sfsi_telegram_page";s:2:"no";s:21:"sfsi_telegram_pageURL";s:0:"";s:21:"sfsi_telegram_message";s:0:"";s:22:"sfsi_telegram_username";s:0:"";s:25:"sfsi_telegram_messageName";s:0:"";s:15:"sfsi_weibo_page";s:2:"no";s:18:"sfsi_weibo_pageURL";s:0:"";s:12:"sfsi_vk_page";s:2:"no";s:15:"sfsi_vk_pageURL";s:0:"";s:12:"sfsi_ok_page";s:2:"no";s:15:"sfsi_ok_pageURL";s:0:"";s:23:"sfsi_wechatShare_option";s:3:"yes";}";', 'yes'),
(179, 'sfsi_section3_options', 's:382:"a:9:{s:14:"sfsi_mouseOver";s:2:"no";s:21:"sfsi_mouseOver_effect";s:7:"fade_in";s:26:"sfsi_mouseOver_effect_type";s:10:"same_icons";s:39:"mouseover_other_icons_transition_effect";s:4:"flip";s:18:"sfsi_shuffle_icons";s:2:"no";s:22:"sfsi_shuffle_Firstload";s:2:"no";s:21:"sfsi_shuffle_interval";s:2:"no";s:25:"sfsi_shuffle_intervalTime";s:0:"";s:18:"sfsi_actvite_theme";s:7:"default";}";', 'yes'),
(180, 'sfsi_section4_options', 's:2309:"a:58:{s:19:"sfsi_display_counts";s:2:"no";s:24:"sfsi_email_countsDisplay";s:2:"no";s:21:"sfsi_email_countsFrom";s:6:"source";s:23:"sfsi_email_manualCounts";s:2:"20";s:22:"sfsi_rss_countsDisplay";s:2:"no";s:21:"sfsi_rss_manualCounts";s:2:"20";s:22:"sfsi_facebook_PageLink";s:0:"";s:27:"sfsi_facebook_countsDisplay";s:2:"no";s:24:"sfsi_facebook_countsFrom";s:6:"manual";s:26:"sfsi_facebook_manualCounts";s:2:"20";s:26:"sfsi_twitter_countsDisplay";s:2:"no";s:23:"sfsi_twitter_countsFrom";s:6:"manual";s:25:"sfsi_twitter_manualCounts";s:2:"20";s:19:"sfsi_google_api_key";s:0:"";s:25:"sfsi_google_countsDisplay";s:2:"no";s:22:"sfsi_google_countsFrom";s:6:"manual";s:24:"sfsi_google_manualCounts";s:2:"20";s:27:"sfsi_linkedIn_countsDisplay";s:2:"no";s:24:"sfsi_linkedIn_countsFrom";s:6:"manual";s:26:"sfsi_linkedIn_manualCounts";s:2:"20";s:27:"sfsi_telegram_countsDisplay";s:2:"no";s:24:"sfsi_telegram_countsFrom";s:6:"manual";s:26:"sfsi_telegram_manualCounts";s:2:"20";s:21:"sfsi_vk_countsDisplay";s:2:"no";s:18:"sfsi_vk_countsFrom";s:6:"manual";s:20:"sfsi_vk_manualCounts";s:2:"20";s:21:"sfsi_ok_countsDisplay";s:2:"no";s:18:"sfsi_ok_countsFrom";s:6:"manual";s:20:"sfsi_ok_manualCounts";s:2:"20";s:24:"sfsi_weibo_countsDisplay";s:2:"no";s:21:"sfsi_weibo_countsFrom";s:6:"manual";s:23:"sfsi_weibo_manualCounts";s:2:"20";s:25:"sfsi_wechat_countsDisplay";s:2:"no";s:22:"sfsi_wechat_countsFrom";s:6:"manual";s:24:"sfsi_wechat_manualCounts";s:2:"20";s:10:"ln_api_key";s:0:"";s:13:"ln_secret_key";s:0:"";s:19:"ln_oAuth_user_token";s:0:"";s:10:"ln_company";s:0:"";s:24:"sfsi_youtubeusernameorid";s:4:"name";s:17:"sfsi_youtube_user";s:0:"";s:22:"sfsi_youtube_channelId";s:0:"";s:17:"sfsi_ytube_chnlid";s:0:"";s:26:"sfsi_youtube_countsDisplay";s:2:"no";s:23:"sfsi_youtube_countsFrom";s:6:"manual";s:25:"sfsi_youtube_manualCounts";s:2:"20";s:28:"sfsi_pinterest_countsDisplay";s:2:"no";s:25:"sfsi_pinterest_countsFrom";s:6:"manual";s:27:"sfsi_pinterest_manualCounts";s:2:"20";s:19:"sfsi_pinterest_user";s:0:"";s:20:"sfsi_pinterest_board";s:0:"";s:25:"sfsi_instagram_countsFrom";s:6:"manual";s:28:"sfsi_instagram_countsDisplay";s:2:"no";s:27:"sfsi_instagram_manualCounts";s:2:"20";s:19:"sfsi_instagram_User";s:0:"";s:23:"sfsi_instagram_clientid";s:0:"";s:21:"sfsi_instagram_appurl";s:0:"";s:20:"sfsi_instagram_token";s:0:"";}";', 'yes'),
(181, 'sfsi_section5_options', 's:1613:"a:39:{s:15:"sfsi_icons_size";s:2:"40";s:18:"sfsi_icons_spacing";s:1:"5";s:20:"sfsi_icons_Alignment";s:4:"left";s:17:"sfsi_icons_perRow";s:1:"5";s:24:"sfsi_icons_ClickPageOpen";s:3:"yes";s:26:"sfsi_icons_suppress_errors";s:2:"no";s:16:"sfsi_icons_stick";s:2:"no";s:18:"sfsi_rssIcon_order";s:1:"1";s:20:"sfsi_emailIcon_order";s:1:"2";s:23:"sfsi_facebookIcon_order";s:1:"3";s:22:"sfsi_twitterIcon_order";s:1:"4";s:22:"sfsi_youtubeIcon_order";s:1:"5";s:24:"sfsi_pinterestIcon_order";s:1:"7";s:23:"sfsi_linkedinIcon_order";s:1:"8";s:24:"sfsi_instagramIcon_order";s:1:"9";s:21:"sfsi_googleIcon_order";s:2:"10";s:23:"sfsi_telegramIcon_order";s:2:"11";s:17:"sfsi_vkIcon_order";s:2:"12";s:17:"sfsi_okIcon_order";s:2:"13";s:20:"sfsi_weiboIcon_order";s:2:"14";s:21:"sfsi_wechatIcon_order";s:2:"15";s:22:"sfsi_CustomIcons_order";s:0:"";s:22:"sfsi_rss_MouseOverText";s:3:"RSS";s:24:"sfsi_email_MouseOverText";s:15:"Follow by Email";s:26:"sfsi_twitter_MouseOverText";s:7:"Twitter";s:27:"sfsi_facebook_MouseOverText";s:8:"Facebook";s:25:"sfsi_google_MouseOverText";s:7:"Google+";s:27:"sfsi_linkedIn_MouseOverText";s:8:"LinkedIn";s:28:"sfsi_pinterest_MouseOverText";s:9:"Pinterest";s:28:"sfsi_instagram_MouseOverText";s:9:"Instagram";s:26:"sfsi_youtube_MouseOverText";s:7:"YouTube";s:27:"sfsi_telegram_MouseOverText";s:8:"Telegram";s:21:"sfsi_vk_MouseOverText";s:2:"VK";s:21:"sfsi_ok_MouseOverText";s:2:"OK";s:24:"sfsi_weibo_MouseOverText";s:5:"Weibo";s:25:"sfsi_wechat_MouseOverText";s:6:"WeChat";s:26:"sfsi_custom_MouseOverTexts";s:0:"";s:23:"sfsi_custom_social_hide";s:2:"no";s:32:"sfsi_pplus_icons_suppress_errors";s:2:"no";}";', 'yes'),
(182, 'sfsi_section6_options', 's:523:"a:14:{s:17:"sfsi_show_Onposts";s:2:"no";s:18:"sfsi_show_Onbottom";s:2:"no";s:22:"sfsi_icons_postPositon";s:6:"source";s:20:"sfsi_icons_alignment";s:12:"center-right";s:22:"sfsi_rss_countsDisplay";s:2:"no";s:20:"sfsi_textBefor_icons";s:26:"Please follow and like us:";s:24:"sfsi_icons_DisplayCounts";s:2:"no";s:12:"sfsi_rectsub";s:3:"yes";s:11:"sfsi_rectfb";s:3:"yes";s:11:"sfsi_rectgp";s:3:"yes";s:12:"sfsi_rectshr";s:2:"no";s:13:"sfsi_recttwtr";s:3:"yes";s:14:"sfsi_rectpinit";s:3:"yes";s:16:"sfsi_rectfbshare";s:3:"yes";}";', 'yes'),
(183, 'sfsi_section7_options', 's:666:"a:15:{s:15:"sfsi_show_popup";s:2:"no";s:15:"sfsi_popup_text";s:42:"Enjoy this blog? Please spread the word :)";s:27:"sfsi_popup_background_color";s:7:"#eff7f7";s:23:"sfsi_popup_border_color";s:7:"#f3faf2";s:27:"sfsi_popup_border_thickness";s:1:"1";s:24:"sfsi_popup_border_shadow";s:3:"yes";s:15:"sfsi_popup_font";s:26:"Helvetica,Arial,sans-serif";s:19:"sfsi_popup_fontSize";s:2:"30";s:20:"sfsi_popup_fontStyle";s:6:"normal";s:20:"sfsi_popup_fontColor";s:7:"#000000";s:17:"sfsi_Show_popupOn";s:4:"none";s:25:"sfsi_Show_popupOn_PageIDs";s:0:"";s:14:"sfsi_Shown_pop";s:8:"ETscroll";s:24:"sfsi_Shown_popupOnceTime";s:0:"";s:32:"sfsi_Shown_popuplimitPerUserTime";s:0:"";}";', 'yes'),
(184, 'sfsi_section8_options', 's:1208:"a:26:{s:20:"sfsi_form_adjustment";s:3:"yes";s:16:"sfsi_form_height";s:3:"180";s:15:"sfsi_form_width";s:3:"230";s:16:"sfsi_form_border";s:2:"no";s:26:"sfsi_form_border_thickness";s:1:"1";s:22:"sfsi_form_border_color";s:7:"#b5b5b5";s:20:"sfsi_form_background";s:7:"#ffffff";s:22:"sfsi_form_heading_text";s:22:"Get new posts by email";s:22:"sfsi_form_heading_font";s:26:"Helvetica,Arial,sans-serif";s:27:"sfsi_form_heading_fontstyle";s:4:"bold";s:27:"sfsi_form_heading_fontcolor";s:7:"#000000";s:26:"sfsi_form_heading_fontsize";s:2:"16";s:27:"sfsi_form_heading_fontalign";s:6:"center";s:20:"sfsi_form_field_text";s:9:"Subscribe";s:20:"sfsi_form_field_font";s:26:"Helvetica,Arial,sans-serif";s:25:"sfsi_form_field_fontstyle";s:6:"normal";s:25:"sfsi_form_field_fontcolor";s:7:"#000000";s:24:"sfsi_form_field_fontsize";s:2:"14";s:25:"sfsi_form_field_fontalign";s:6:"center";s:21:"sfsi_form_button_text";s:9:"Subscribe";s:21:"sfsi_form_button_font";s:26:"Helvetica,Arial,sans-serif";s:26:"sfsi_form_button_fontstyle";s:4:"bold";s:26:"sfsi_form_button_fontcolor";s:7:"#000000";s:25:"sfsi_form_button_fontsize";s:2:"16";s:26:"sfsi_form_button_fontalign";s:6:"center";s:27:"sfsi_form_button_background";s:7:"#dedede";}";', 'yes'),
(185, 'sfsi_section9_options', 's:420:"a:10:{s:20:"sfsi_show_via_widget";s:2:"no";s:16:"sfsi_icons_float";s:2:"no";s:24:"sfsi_icons_floatPosition";s:12:"center-right";s:26:"sfsi_icons_floatMargin_top";s:0:"";s:29:"sfsi_icons_floatMargin_bottom";s:0:"";s:27:"sfsi_icons_floatMargin_left";s:0:"";s:28:"sfsi_icons_floatMargin_right";s:0:"";s:23:"sfsi_disable_floaticons";s:2:"no";s:23:"sfsi_show_via_shortcode";s:2:"no";s:24:"sfsi_show_via_afterposts";s:2:"no";}";', 'yes'),
(186, 'sfsi_feed_id', '', 'yes'),
(187, 'sfsi_redirect_url', '', 'yes'),
(188, 'sfsi_installDate', '2019-09-23 11:26:03', 'yes'),
(189, 'sfsi_RatingDiv', 'no', 'yes'),
(190, 'sfsi_footer_sec', 'no', 'yes'),
(191, 'sfsi_activate', '0', 'yes'),
(192, 'sfsi_instagram_sf_count', 's:131:"a:4:{s:7:"date_sf";i:1569196800;s:14:"date_instagram";i:1569196800;s:13:"sfsi_sf_count";s:0:"";s:20:"sfsi_instagram_count";s:0:"";}";', 'yes'),
(193, 'sfsi_error_reporting_notice_dismissed', '1', 'yes'),
(194, 'adding_tags', 'no', 'yes'),
(195, 'widget_sfsi-widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(196, 'widget_subscriber_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(197, 'sfsi_pluginVersion', '2.24', 'yes'),
(198, 'sfsi_serverphpVersionnotification', 'yes', 'yes'),
(199, 'show_premium_notification', 'yes', 'yes'),
(200, 'show_notification', 'yes', 'yes'),
(201, 'show_mobile_notification', 'yes', 'yes'),
(202, 'sfsi_languageNotice', 'yes', 'yes'),
(203, 'sfsi_pplus_error_reporting_notice_dismissed', '1', 'yes'),
(204, 'sfsi_addThis_icon_removal_notice_dismissed', '1', 'yes'),
(205, 'wpseo', 'a:20:{s:15:"ms_defaults_set";b:0;s:7:"version";s:4:"11.5";s:20:"disableadvanced_meta";b:1;s:19:"onpage_indexability";b:1;s:11:"baiduverify";s:0:"";s:12:"googleverify";s:0:"";s:8:"msverify";s:0:"";s:12:"yandexverify";s:0:"";s:9:"site_type";s:0:"";s:20:"has_multiple_authors";s:0:"";s:16:"environment_type";s:0:"";s:23:"content_analysis_active";b:1;s:23:"keyword_analysis_active";b:1;s:21:"enable_admin_bar_menu";b:1;s:26:"enable_cornerstone_content";b:1;s:18:"enable_xml_sitemap";b:1;s:24:"enable_text_link_counter";b:1;s:22:"show_onboarding_notice";b:1;s:18:"first_activated_on";i:1569238016;s:13:"myyoast-oauth";b:0;}', 'yes'),
(206, 'wpseo_titles', 'a:71:{s:10:"title_test";i:0;s:17:"forcerewritetitle";b:0;s:9:"separator";s:7:"sc-dash";s:16:"title-home-wpseo";s:42:"%%sitename%% %%page%% %%sep%% %%sitedesc%%";s:18:"title-author-wpseo";s:41:"%%name%%, Author at %%sitename%% %%page%%";s:19:"title-archive-wpseo";s:38:"%%date%% %%page%% %%sep%% %%sitename%%";s:18:"title-search-wpseo";s:63:"You searched for %%searchphrase%% %%page%% %%sep%% %%sitename%%";s:15:"title-404-wpseo";s:35:"Page not found %%sep%% %%sitename%%";s:19:"metadesc-home-wpseo";s:0:"";s:21:"metadesc-author-wpseo";s:0:"";s:22:"metadesc-archive-wpseo";s:0:"";s:9:"rssbefore";s:0:"";s:8:"rssafter";s:53:"The post %%POSTLINK%% appeared first on %%BLOGLINK%%.";s:20:"noindex-author-wpseo";b:0;s:28:"noindex-author-noposts-wpseo";b:1;s:21:"noindex-archive-wpseo";b:1;s:14:"disable-author";b:0;s:12:"disable-date";b:0;s:19:"disable-post_format";b:0;s:18:"disable-attachment";b:1;s:23:"is-media-purge-relevant";b:0;s:20:"breadcrumbs-404crumb";s:25:"Error 404: Page not found";s:29:"breadcrumbs-display-blog-page";b:1;s:20:"breadcrumbs-boldlast";b:0;s:25:"breadcrumbs-archiveprefix";s:12:"Archives for";s:18:"breadcrumbs-enable";b:0;s:16:"breadcrumbs-home";s:4:"Home";s:18:"breadcrumbs-prefix";s:0:"";s:24:"breadcrumbs-searchprefix";s:16:"You searched for";s:15:"breadcrumbs-sep";s:7:"&raquo;";s:12:"website_name";s:0:"";s:11:"person_name";s:0:"";s:11:"person_logo";s:0:"";s:14:"person_logo_id";i:0;s:22:"alternate_website_name";s:0:"";s:12:"company_logo";s:0:"";s:15:"company_logo_id";i:0;s:12:"company_name";s:0:"";s:17:"company_or_person";s:7:"company";s:25:"company_or_person_user_id";b:0;s:17:"stripcategorybase";b:0;s:10:"title-post";s:39:"%%title%% %%page%% %%sep%% %%sitename%%";s:13:"metadesc-post";s:0:"";s:12:"noindex-post";b:0;s:13:"showdate-post";b:0;s:23:"display-metabox-pt-post";b:1;s:23:"post_types-post-maintax";i:0;s:10:"title-page";s:39:"%%title%% %%page%% %%sep%% %%sitename%%";s:13:"metadesc-page";s:0:"";s:12:"noindex-page";b:0;s:13:"showdate-page";b:0;s:23:"display-metabox-pt-page";b:1;s:23:"post_types-page-maintax";i:0;s:16:"title-attachment";s:39:"%%title%% %%page%% %%sep%% %%sitename%%";s:19:"metadesc-attachment";s:0:"";s:18:"noindex-attachment";b:0;s:19:"showdate-attachment";b:0;s:29:"display-metabox-pt-attachment";b:1;s:29:"post_types-attachment-maintax";i:0;s:18:"title-tax-category";s:53:"%%term_title%% Archives %%page%% %%sep%% %%sitename%%";s:21:"metadesc-tax-category";s:0:"";s:28:"display-metabox-tax-category";b:1;s:20:"noindex-tax-category";b:0;s:18:"title-tax-post_tag";s:53:"%%term_title%% Archives %%page%% %%sep%% %%sitename%%";s:21:"metadesc-tax-post_tag";s:0:"";s:28:"display-metabox-tax-post_tag";b:1;s:20:"noindex-tax-post_tag";b:0;s:21:"title-tax-post_format";s:53:"%%term_title%% Archives %%page%% %%sep%% %%sitename%%";s:24:"metadesc-tax-post_format";s:0:"";s:31:"display-metabox-tax-post_format";b:1;s:23:"noindex-tax-post_format";b:1;}', 'yes'),
(207, 'wpseo_social', 'a:19:{s:13:"facebook_site";s:0:"";s:13:"instagram_url";s:0:"";s:12:"linkedin_url";s:0:"";s:11:"myspace_url";s:0:"";s:16:"og_default_image";s:0:"";s:19:"og_default_image_id";s:0:"";s:18:"og_frontpage_title";s:0:"";s:17:"og_frontpage_desc";s:0:"";s:18:"og_frontpage_image";s:0:"";s:21:"og_frontpage_image_id";s:0:"";s:9:"opengraph";b:1;s:13:"pinterest_url";s:0:"";s:15:"pinterestverify";s:0:"";s:7:"twitter";b:1;s:12:"twitter_site";s:0:"";s:17:"twitter_card_type";s:19:"summary_large_image";s:11:"youtube_url";s:0:"";s:13:"wikipedia_url";s:0:"";s:10:"fbadminapp";s:0:"";}', 'yes'),
(208, 'wpseo_flush_rewrite', '1', 'yes'),
(209, '_transient_timeout_wpseo_link_table_inaccessible', '1600774016', 'no'),
(210, '_transient_wpseo_link_table_inaccessible', '0', 'no'),
(211, '_transient_timeout_wpseo_meta_table_inaccessible', '1600774016', 'no'),
(212, '_transient_wpseo_meta_table_inaccessible', '0', 'no'),
(214, 'rewrite_rules', 'a:115:{s:19:"sitemap_index\\.xml$";s:19:"index.php?sitemap=1";s:31:"([^/]+?)-sitemap([0-9]+)?\\.xml$";s:51:"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]";s:24:"([a-z]+)?-?sitemap\\.xsl$";s:39:"index.php?yoast-sitemap-xsl=$matches[1]";s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:57:"wpforms_log_type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:55:"index.php?wpforms_log_type=$matches[1]&feed=$matches[2]";s:52:"wpforms_log_type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:55:"index.php?wpforms_log_type=$matches[1]&feed=$matches[2]";s:33:"wpforms_log_type/([^/]+)/embed/?$";s:49:"index.php?wpforms_log_type=$matches[1]&embed=true";s:45:"wpforms_log_type/([^/]+)/page/?([0-9]{1,})/?$";s:56:"index.php?wpforms_log_type=$matches[1]&paged=$matches[2]";s:27:"wpforms_log_type/([^/]+)/?$";s:38:"index.php?wpforms_log_type=$matches[1]";s:44:"amn_wpforms-lite/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:54:"amn_wpforms-lite/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:74:"amn_wpforms-lite/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:69:"amn_wpforms-lite/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:69:"amn_wpforms-lite/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:50:"amn_wpforms-lite/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:33:"amn_wpforms-lite/([^/]+)/embed/?$";s:49:"index.php?amn_wpforms-lite=$matches[1]&embed=true";s:37:"amn_wpforms-lite/([^/]+)/trackback/?$";s:43:"index.php?amn_wpforms-lite=$matches[1]&tb=1";s:45:"amn_wpforms-lite/([^/]+)/page/?([0-9]{1,})/?$";s:56:"index.php?amn_wpforms-lite=$matches[1]&paged=$matches[2]";s:52:"amn_wpforms-lite/([^/]+)/comment-page-([0-9]{1,})/?$";s:56:"index.php?amn_wpforms-lite=$matches[1]&cpage=$matches[2]";s:41:"amn_wpforms-lite/([^/]+)(?:/([0-9]+))?/?$";s:55:"index.php?amn_wpforms-lite=$matches[1]&page=$matches[2]";s:33:"amn_wpforms-lite/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:43:"amn_wpforms-lite/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:63:"amn_wpforms-lite/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"amn_wpforms-lite/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"amn_wpforms-lite/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:39:"amn_wpforms-lite/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:39:"index.php?&page_id=12&cpage=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:58:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:68:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:88:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:83:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:64:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:53:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$";s:91:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$";s:85:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1";s:77:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:65:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]";s:72:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]";s:61:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]";s:47:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:57:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:77:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:53:"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]";s:51:"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]";s:38:"([0-9]{4})/comment-page-([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&cpage=$matches[2]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";}', 'yes'),
(221, 'category_children', 'a:0:{}', 'yes'),
(240, 'widget_shark_business_introduction_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(241, 'widget_shark_business_featured_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(242, 'widget_shark_business_portfolio_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(243, 'widget_shark_business_recent_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(244, 'widget_shark_business_service_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(245, 'widget_shark_business_cta_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(246, 'widget_shark_business_client_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(247, 'widget_shark_business_social_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(248, 'theme_mods_shark-business', 'a:4:{s:18:"custom_css_post_id";i:-1;s:18:"nav_menu_locations";a:1:{s:7:"primary";i:2;}s:28:"shark_business_theme_options";a:7:{s:21:"slider_content_page_1";i:12;s:21:"slider_content_page_2";i:10;s:21:"slider_content_page_3";i:16;s:21:"sidebar_single_layout";s:13:"right-sidebar";s:19:"sidebar_page_layout";s:13:"right-sidebar";s:20:"enable_sticky_header";b:0;s:18:"slider_entire_site";b:1;}s:12:"header_image";s:21:"random-uploaded-image";}', 'yes'),
(249, 'theme_switch_menu_locations', 'a:2:{s:6:"menu-1";i:2;s:6:"footer";i:2;}', 'yes'),
(250, 'current_theme', 'Shark Business', 'yes'),
(251, 'theme_switched', '', 'yes'),
(252, 'theme_switched_via_customizer', '', 'yes'),
(253, 'customize_stashed_theme_mods', 'a:0:{}', 'no'),
(272, '_site_transient_timeout_theme_roots', '1569566764', 'no'),
(273, '_site_transient_theme_roots', 'a:4:{s:14:"shark-business";s:7:"/themes";s:14:"twentynineteen";s:7:"/themes";s:15:"twentyseventeen";s:7:"/themes";s:13:"twentysixteen";s:7:"/themes";}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(274, '_site_transient_update_plugins', 'O:8:"stdClass":5:{s:12:"last_checked";i:1569564969;s:7:"checked";a:9:{s:19:"akismet/akismet.php";s:5:"4.1.2";s:23:"grid-plus/grid-plus.php";s:5:"1.2.5";s:9:"hello.php";s:5:"1.7.2";s:23:"ml-slider/ml-slider.php";s:6:"3.14.0";s:47:"regenerate-thumbnails/regenerate-thumbnails.php";s:5:"3.1.1";s:49:"restricted-site-access/restricted_site_access.php";s:5:"7.1.0";s:59:"ultimate-social-media-icons/ultimate_social_media_icons.php";s:5:"2.2.5";s:24:"wpforms-lite/wpforms.php";s:7:"1.5.3.1";s:24:"wordpress-seo/wp-seo.php";s:4:"11.5";}s:8:"response";a:3:{s:59:"ultimate-social-media-icons/ultimate_social_media_icons.php";O:8:"stdClass":13:{s:2:"id";s:41:"w.org/plugins/ultimate-social-media-icons";s:4:"slug";s:27:"ultimate-social-media-icons";s:6:"plugin";s:59:"ultimate-social-media-icons/ultimate_social_media_icons.php";s:11:"new_version";s:5:"2.4.2";s:3:"url";s:58:"https://wordpress.org/plugins/ultimate-social-media-icons/";s:7:"package";s:75:"http://downloads.wordpress.org/plugin/ultimate-social-media-icons.2.4.2.zip";s:5:"icons";a:1:{s:2:"1x";s:80:"https://ps.w.org/ultimate-social-media-icons/assets/icon-128x128.png?rev=1598977";}s:7:"banners";a:1:{s:2:"1x";s:82:"https://ps.w.org/ultimate-social-media-icons/assets/banner-772x250.png?rev=1032920";}s:11:"banners_rtl";a:0:{}s:14:"upgrade_notice";s:21:"<p>Please upgrade</p>";s:6:"tested";s:5:"5.2.3";s:12:"requires_php";b:0;s:13:"compatibility";O:8:"stdClass":0:{}}s:24:"wpforms-lite/wpforms.php";O:8:"stdClass":12:{s:2:"id";s:26:"w.org/plugins/wpforms-lite";s:4:"slug";s:12:"wpforms-lite";s:6:"plugin";s:24:"wpforms-lite/wpforms.php";s:11:"new_version";s:7:"1.5.5.1";s:3:"url";s:43:"https://wordpress.org/plugins/wpforms-lite/";s:7:"package";s:62:"http://downloads.wordpress.org/plugin/wpforms-lite.1.5.5.1.zip";s:5:"icons";a:2:{s:2:"2x";s:65:"https://ps.w.org/wpforms-lite/assets/icon-256x256.png?rev=1371112";s:2:"1x";s:65:"https://ps.w.org/wpforms-lite/assets/icon-128x128.png?rev=1371112";}s:7:"banners";a:2:{s:2:"2x";s:68:"https://ps.w.org/wpforms-lite/assets/banner-1544x500.png?rev=1371112";s:2:"1x";s:67:"https://ps.w.org/wpforms-lite/assets/banner-772x250.png?rev=1371112";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"5.2.3";s:12:"requires_php";s:5:"5.3.3";s:13:"compatibility";O:8:"stdClass":0:{}}s:24:"wordpress-seo/wp-seo.php";O:8:"stdClass":12:{s:2:"id";s:27:"w.org/plugins/wordpress-seo";s:4:"slug";s:13:"wordpress-seo";s:6:"plugin";s:24:"wordpress-seo/wp-seo.php";s:11:"new_version";s:4:"12.1";s:3:"url";s:44:"https://wordpress.org/plugins/wordpress-seo/";s:7:"package";s:60:"http://downloads.wordpress.org/plugin/wordpress-seo.12.1.zip";s:5:"icons";a:3:{s:2:"2x";s:66:"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1834347";s:2:"1x";s:58:"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641";s:3:"svg";s:58:"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641";}s:7:"banners";a:2:{s:2:"2x";s:69:"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435";s:2:"1x";s:68:"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435";}s:11:"banners_rtl";a:2:{s:2:"2x";s:73:"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435";s:2:"1x";s:72:"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435";}s:6:"tested";s:5:"5.2.3";s:12:"requires_php";s:5:"5.2.4";s:13:"compatibility";O:8:"stdClass":0:{}}}s:12:"translations";a:0:{}s:9:"no_update";a:6:{s:19:"akismet/akismet.php";O:8:"stdClass":9:{s:2:"id";s:21:"w.org/plugins/akismet";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:5:"4.1.2";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:55:"http://downloads.wordpress.org/plugin/akismet.4.1.2.zip";s:5:"icons";a:2:{s:2:"2x";s:59:"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272";s:2:"1x";s:59:"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272";}s:7:"banners";a:1:{s:2:"1x";s:61:"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904";}s:11:"banners_rtl";a:0:{}}s:23:"grid-plus/grid-plus.php";O:8:"stdClass":9:{s:2:"id";s:23:"w.org/plugins/grid-plus";s:4:"slug";s:9:"grid-plus";s:6:"plugin";s:23:"grid-plus/grid-plus.php";s:11:"new_version";s:5:"1.2.5";s:3:"url";s:40:"https://wordpress.org/plugins/grid-plus/";s:7:"package";s:57:"http://downloads.wordpress.org/plugin/grid-plus.1.2.5.zip";s:5:"icons";a:1:{s:2:"1x";s:62:"https://ps.w.org/grid-plus/assets/icon-128x128.png?rev=1598458";}s:7:"banners";a:1:{s:2:"1x";s:64:"https://ps.w.org/grid-plus/assets/banner-772x250.png?rev=1598461";}s:11:"banners_rtl";a:0:{}}s:9:"hello.php";O:8:"stdClass":9:{s:2:"id";s:25:"w.org/plugins/hello-dolly";s:4:"slug";s:11:"hello-dolly";s:6:"plugin";s:9:"hello.php";s:11:"new_version";s:5:"1.7.2";s:3:"url";s:42:"https://wordpress.org/plugins/hello-dolly/";s:7:"package";s:59:"http://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip";s:5:"icons";a:2:{s:2:"2x";s:64:"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855";s:2:"1x";s:64:"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855";}s:7:"banners";a:1:{s:2:"1x";s:66:"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855";}s:11:"banners_rtl";a:0:{}}s:23:"ml-slider/ml-slider.php";O:8:"stdClass":9:{s:2:"id";s:23:"w.org/plugins/ml-slider";s:4:"slug";s:9:"ml-slider";s:6:"plugin";s:23:"ml-slider/ml-slider.php";s:11:"new_version";s:6:"3.14.0";s:3:"url";s:40:"https://wordpress.org/plugins/ml-slider/";s:7:"package";s:58:"http://downloads.wordpress.org/plugin/ml-slider.3.14.0.zip";s:5:"icons";a:3:{s:2:"2x";s:62:"https://ps.w.org/ml-slider/assets/icon-256x256.png?rev=1837669";s:2:"1x";s:54:"https://ps.w.org/ml-slider/assets/icon.svg?rev=1837669";s:3:"svg";s:54:"https://ps.w.org/ml-slider/assets/icon.svg?rev=1837669";}s:7:"banners";a:2:{s:2:"2x";s:65:"https://ps.w.org/ml-slider/assets/banner-1544x500.png?rev=1837669";s:2:"1x";s:64:"https://ps.w.org/ml-slider/assets/banner-772x250.png?rev=1837669";}s:11:"banners_rtl";a:0:{}}s:47:"regenerate-thumbnails/regenerate-thumbnails.php";O:8:"stdClass":9:{s:2:"id";s:35:"w.org/plugins/regenerate-thumbnails";s:4:"slug";s:21:"regenerate-thumbnails";s:6:"plugin";s:47:"regenerate-thumbnails/regenerate-thumbnails.php";s:11:"new_version";s:5:"3.1.1";s:3:"url";s:52:"https://wordpress.org/plugins/regenerate-thumbnails/";s:7:"package";s:69:"http://downloads.wordpress.org/plugin/regenerate-thumbnails.3.1.1.zip";s:5:"icons";a:1:{s:2:"1x";s:74:"https://ps.w.org/regenerate-thumbnails/assets/icon-128x128.png?rev=1753390";}s:7:"banners";a:2:{s:2:"2x";s:77:"https://ps.w.org/regenerate-thumbnails/assets/banner-1544x500.jpg?rev=1753390";s:2:"1x";s:76:"https://ps.w.org/regenerate-thumbnails/assets/banner-772x250.jpg?rev=1753390";}s:11:"banners_rtl";a:0:{}}s:49:"restricted-site-access/restricted_site_access.php";O:8:"stdClass":9:{s:2:"id";s:36:"w.org/plugins/restricted-site-access";s:4:"slug";s:22:"restricted-site-access";s:6:"plugin";s:49:"restricted-site-access/restricted_site_access.php";s:11:"new_version";s:5:"7.1.0";s:3:"url";s:53:"https://wordpress.org/plugins/restricted-site-access/";s:7:"package";s:70:"http://downloads.wordpress.org/plugin/restricted-site-access.7.1.0.zip";s:5:"icons";a:2:{s:2:"2x";s:75:"https://ps.w.org/restricted-site-access/assets/icon-256x256.png?rev=2138472";s:2:"1x";s:75:"https://ps.w.org/restricted-site-access/assets/icon-128x128.png?rev=2138472";}s:7:"banners";a:2:{s:2:"2x";s:78:"https://ps.w.org/restricted-site-access/assets/banner-1544x500.png?rev=2138472";s:2:"1x";s:77:"https://ps.w.org/restricted-site-access/assets/banner-772x250.png?rev=2138472";}s:11:"banners_rtl";a:0:{}}}}', 'no'),
(275, '_transient_timeout_wpforms_dash_widget_lite_entries_by_form', '1569628802', 'no'),
(276, '_transient_wpforms_dash_widget_lite_entries_by_form', 'a:1:{i:15;a:3:{s:7:"form_id";i:15;s:5:"count";i:0;s:5:"title";s:19:"Simple Contact Form";}}', 'no'),
(277, '_transient_timeout_wpseo-statistics-totals', '1569651404', 'no'),
(278, '_transient_wpseo-statistics-totals', 'a:1:{i:1;a:2:{s:6:"scores";a:1:{i:0;a:4:{s:8:"seo_rank";s:2:"na";s:5:"label";s:48:"Posts <strong>without</strong> a focus keyphrase";s:5:"count";i:4;s:4:"link";s:107:"http://localhost/profile.local/wp-admin/edit.php?post_status=publish&#038;post_type=post&#038;seo_filter=na";}}s:8:"division";a:5:{s:3:"bad";i:0;s:2:"ok";i:0;s:4:"good";i:0;s:2:"na";i:1;s:7:"noindex";i:0;}}}', 'no'),
(279, '_transient_timeout_dash_v2_88ae138922fe95674369b1cb3d215a2b', '1569608206', 'no'),
(280, '_transient_dash_v2_88ae138922fe95674369b1cb3d215a2b', '<div class="rss-widget"><p><strong>RSS Error:</strong> WP HTTP Error: cURL error 60: SSL certificate problem: self signed certificate in certificate chain</p></div><div class="rss-widget"><p><strong>RSS Error:</strong> WP HTTP Error: cURL error 60: SSL certificate problem: self signed certificate in certificate chain</p></div>', 'no'),
(281, '_transient_timeout_plugin_slugs', '1569651443', 'no'),
(282, '_transient_plugin_slugs', 'a:9:{i:0;s:19:"akismet/akismet.php";i:1;s:23:"grid-plus/grid-plus.php";i:2;s:9:"hello.php";i:3;s:23:"ml-slider/ml-slider.php";i:4;s:47:"regenerate-thumbnails/regenerate-thumbnails.php";i:5;s:49:"restricted-site-access/restricted_site_access.php";i:6;s:59:"ultimate-social-media-icons/ultimate_social_media_icons.php";i:7;s:24:"wpforms-lite/wpforms.php";i:8;s:24:"wordpress-seo/wp-seo.php";}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE IF NOT EXISTS `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB AUTO_INCREMENT=237 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_menu_item_type', 'custom'),
(4, 5, '_menu_item_menu_item_parent', '0'),
(5, 5, '_menu_item_object_id', '5'),
(6, 5, '_menu_item_object', 'custom'),
(7, 5, '_menu_item_target', ''),
(8, 5, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(9, 5, '_menu_item_xfn', ''),
(10, 5, '_menu_item_url', '/'),
(11, 5, '_menu_item_orphaned', '1569232994'),
(12, 6, '_menu_item_type', 'custom'),
(13, 6, '_menu_item_menu_item_parent', '0'),
(14, 6, '_menu_item_object_id', '6'),
(15, 6, '_menu_item_object', 'custom'),
(16, 6, '_menu_item_target', ''),
(17, 6, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(18, 6, '_menu_item_xfn', ''),
(19, 6, '_menu_item_url', '/home'),
(21, 7, '_menu_item_type', 'custom'),
(22, 7, '_menu_item_menu_item_parent', '0'),
(23, 7, '_menu_item_object_id', '7'),
(24, 7, '_menu_item_object', 'custom'),
(25, 7, '_menu_item_target', ''),
(26, 7, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(27, 7, '_menu_item_xfn', ''),
(28, 7, '_menu_item_url', '/about'),
(30, 8, '_menu_item_type', 'custom'),
(31, 8, '_menu_item_menu_item_parent', '0'),
(32, 8, '_menu_item_object_id', '8'),
(33, 8, '_menu_item_object', 'custom'),
(34, 8, '_menu_item_target', ''),
(35, 8, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(36, 8, '_menu_item_xfn', ''),
(37, 8, '_menu_item_url', '/contact'),
(39, 9, '_menu_item_type', 'custom'),
(40, 9, '_menu_item_menu_item_parent', '0'),
(41, 9, '_menu_item_object_id', '9'),
(42, 9, '_menu_item_object', 'custom'),
(43, 9, '_menu_item_target', ''),
(44, 9, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(45, 9, '_menu_item_xfn', ''),
(46, 9, '_menu_item_url', '/services'),
(48, 10, '_edit_lock', '1569245303:1'),
(49, 12, '_edit_lock', '1569245950:1'),
(50, 2, '_edit_lock', '1569233248:1'),
(51, 14, '_edit_lock', '1569233273:1'),
(52, 16, '_edit_lock', '1569233521:1'),
(53, 18, '_wp_trash_meta_status', 'publish'),
(54, 18, '_wp_trash_meta_time', '1569233688'),
(55, 19, '_wp_trash_meta_status', 'publish'),
(56, 19, '_wp_trash_meta_time', '1569233697'),
(57, 20, '_wp_trash_meta_status', 'publish'),
(58, 20, '_wp_trash_meta_time', '1569233718'),
(59, 21, '_wp_trash_meta_status', 'publish'),
(60, 21, '_wp_trash_meta_time', '1569233739'),
(61, 22, '_wp_trash_meta_status', 'publish'),
(62, 22, '_wp_trash_meta_time', '1569233784'),
(63, 10, '_edit_last', '1'),
(64, 12, '_edit_last', '1'),
(65, 23, 'ml-slider_settings', 'a:38:{s:4:"type";s:4:"flex";s:6:"random";s:5:"false";s:8:"cssClass";s:0:"";s:8:"printCss";s:4:"true";s:7:"printJs";s:4:"true";s:5:"width";s:4:"1920";s:6:"height";s:3:"800";s:3:"spw";i:7;s:3:"sph";i:5;s:5:"delay";s:4:"2000";s:6:"sDelay";s:2:"30";s:7:"opacity";s:3:"0.7";s:10:"titleSpeed";s:3:"500";s:6:"effect";s:4:"fade";s:10:"navigation";s:4:"true";s:5:"links";s:4:"true";s:10:"hoverPause";s:4:"true";s:5:"theme";s:7:"default";s:9:"direction";s:10:"horizontal";s:7:"reverse";s:5:"false";s:14:"animationSpeed";s:3:"400";s:8:"prevText";s:8:"Previous";s:8:"nextText";s:4:"Next";s:6:"slices";i:15;s:6:"center";s:5:"false";s:9:"smartCrop";s:4:"true";s:12:"carouselMode";s:5:"false";s:14:"carouselMargin";s:1:"5";s:16:"firstSlideFadeIn";s:5:"false";s:6:"easing";s:6:"linear";s:8:"autoPlay";s:4:"true";s:11:"thumb_width";i:150;s:12:"thumb_height";i:100;s:17:"responsive_thumbs";s:5:"false";s:15:"thumb_min_width";i:100;s:9:"fullWidth";s:4:"true";s:10:"noConflict";s:4:"true";s:12:"smoothHeight";s:5:"false";}'),
(66, 23, 'metaslider_slideshow_theme', ''),
(67, 25, '_wp_attached_file', '2019/09/Nemo-HPB_1400x438_A1.jpg'),
(68, 25, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1400;s:6:"height";i:438;s:4:"file";s:32:"2019/09/Nemo-HPB_1400x438_A1.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:32:"Nemo-HPB_1400x438_A1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:31:"Nemo-HPB_1400x438_A1-300x94.jpg";s:5:"width";i:300;s:6:"height";i:94;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:32:"Nemo-HPB_1400x438_A1-768x240.jpg";s:5:"width";i:768;s:6:"height";i:240;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:33:"Nemo-HPB_1400x438_A1-1024x320.jpg";s:5:"width";i:1024;s:6:"height";i:320;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-700x300";a:4:{s:4:"file";s:32:"Nemo-HPB_1400x438_A1-700x300.jpg";s:5:"width";i:700;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-620x266";a:4:{s:4:"file";s:32:"Nemo-HPB_1400x438_A1-620x266.jpg";s:5:"width";i:620;s:6:"height";i:266;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-400x171";a:4:{s:4:"file";s:32:"Nemo-HPB_1400x438_A1-400x171.jpg";s:5:"width";i:400;s:6:"height";i:171;s:9:"mime-type";s:10:"image/jpeg";}s:28:"meta-slider-resized-1051x438";a:4:{s:4:"file";s:33:"Nemo-HPB_1400x438_A1-1051x438.jpg";s:5:"width";i:1051;s:6:"height";i:438;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-620x258";a:4:{s:4:"file";s:32:"Nemo-HPB_1400x438_A1-620x258.jpg";s:5:"width";i:620;s:6:"height";i:258;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-400x167";a:4:{s:4:"file";s:32:"Nemo-HPB_1400x438_A1-400x167.jpg";s:5:"width";i:400;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(69, 26, '_wp_attached_file', '2019/09/AFP-HPB_1400x438.jpg'),
(70, 26, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1400;s:6:"height";i:438;s:4:"file";s:28:"2019/09/AFP-HPB_1400x438.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:28:"AFP-HPB_1400x438-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:27:"AFP-HPB_1400x438-300x94.jpg";s:5:"width";i:300;s:6:"height";i:94;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:28:"AFP-HPB_1400x438-768x240.jpg";s:5:"width";i:768;s:6:"height";i:240;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:29:"AFP-HPB_1400x438-1024x320.jpg";s:5:"width";i:1024;s:6:"height";i:320;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-700x300";a:4:{s:4:"file";s:28:"AFP-HPB_1400x438-700x300.jpg";s:5:"width";i:700;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-620x266";a:4:{s:4:"file";s:28:"AFP-HPB_1400x438-620x266.jpg";s:5:"width";i:620;s:6:"height";i:266;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-400x171";a:4:{s:4:"file";s:28:"AFP-HPB_1400x438-400x171.jpg";s:5:"width";i:400;s:6:"height";i:171;s:9:"mime-type";s:10:"image/jpeg";}s:28:"meta-slider-resized-1051x438";a:4:{s:4:"file";s:29:"AFP-HPB_1400x438-1051x438.jpg";s:5:"width";i:1051;s:6:"height";i:438;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-620x258";a:4:{s:4:"file";s:28:"AFP-HPB_1400x438-620x258.jpg";s:5:"width";i:620;s:6:"height";i:258;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-400x167";a:4:{s:4:"file";s:28:"AFP-HPB_1400x438-400x167.jpg";s:5:"width";i:400;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(71, 27, '_wp_attached_file', '2019/09/Nemo-HPB_1400x438_Q3.jpg'),
(72, 27, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1400;s:6:"height";i:438;s:4:"file";s:32:"2019/09/Nemo-HPB_1400x438_Q3.jpg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:32:"Nemo-HPB_1400x438_Q3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:31:"Nemo-HPB_1400x438_Q3-300x94.jpg";s:5:"width";i:300;s:6:"height";i:94;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:32:"Nemo-HPB_1400x438_Q3-768x240.jpg";s:5:"width";i:768;s:6:"height";i:240;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:33:"Nemo-HPB_1400x438_Q3-1024x320.jpg";s:5:"width";i:1024;s:6:"height";i:320;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-700x300";a:4:{s:4:"file";s:32:"Nemo-HPB_1400x438_Q3-700x300.jpg";s:5:"width";i:700;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-620x266";a:4:{s:4:"file";s:32:"Nemo-HPB_1400x438_Q3-620x266.jpg";s:5:"width";i:620;s:6:"height";i:266;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-400x171";a:4:{s:4:"file";s:32:"Nemo-HPB_1400x438_Q3-400x171.jpg";s:5:"width";i:400;s:6:"height";i:171;s:9:"mime-type";s:10:"image/jpeg";}s:28:"meta-slider-resized-1051x438";a:4:{s:4:"file";s:33:"Nemo-HPB_1400x438_Q3-1051x438.jpg";s:5:"width";i:1051;s:6:"height";i:438;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-620x258";a:4:{s:4:"file";s:32:"Nemo-HPB_1400x438_Q3-620x258.jpg";s:5:"width";i:620;s:6:"height";i:258;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-400x167";a:4:{s:4:"file";s:32:"Nemo-HPB_1400x438_Q3-400x167.jpg";s:5:"width";i:400;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(73, 28, '_thumbnail_id', '86'),
(74, 28, 'ml-slider_type', 'image'),
(75, 28, 'ml-slider_inherit_image_title', '1'),
(76, 28, 'ml-slider_inherit_image_alt', '1'),
(77, 29, '_thumbnail_id', '90'),
(78, 29, 'ml-slider_type', 'image'),
(79, 29, 'ml-slider_inherit_image_title', '1'),
(80, 29, 'ml-slider_inherit_image_alt', '1'),
(81, 30, '_thumbnail_id', '82'),
(82, 30, 'ml-slider_type', 'image'),
(83, 30, 'ml-slider_inherit_image_title', '1'),
(84, 30, 'ml-slider_inherit_image_alt', '1'),
(85, 25, '_wp_attachment_backup_sizes', 'a:6:{s:15:"resized-700x300";a:5:{s:4:"path";s:83:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/Nemo-HPB_1400x438_A1-700x300.jpg";s:4:"file";s:32:"Nemo-HPB_1400x438_A1-700x300.jpg";s:5:"width";i:700;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-620x266";a:5:{s:4:"path";s:83:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/Nemo-HPB_1400x438_A1-620x266.jpg";s:4:"file";s:32:"Nemo-HPB_1400x438_A1-620x266.jpg";s:5:"width";i:620;s:6:"height";i:266;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-400x171";a:5:{s:4:"path";s:83:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/Nemo-HPB_1400x438_A1-400x171.jpg";s:4:"file";s:32:"Nemo-HPB_1400x438_A1-400x171.jpg";s:5:"width";i:400;s:6:"height";i:171;s:9:"mime-type";s:10:"image/jpeg";}s:16:"resized-1051x438";a:5:{s:4:"path";s:84:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/Nemo-HPB_1400x438_A1-1051x438.jpg";s:4:"file";s:33:"Nemo-HPB_1400x438_A1-1051x438.jpg";s:5:"width";i:1051;s:6:"height";i:438;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-620x258";a:5:{s:4:"path";s:83:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/Nemo-HPB_1400x438_A1-620x258.jpg";s:4:"file";s:32:"Nemo-HPB_1400x438_A1-620x258.jpg";s:5:"width";i:620;s:6:"height";i:258;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-400x167";a:5:{s:4:"path";s:83:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/Nemo-HPB_1400x438_A1-400x167.jpg";s:4:"file";s:32:"Nemo-HPB_1400x438_A1-400x167.jpg";s:5:"width";i:400;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}}'),
(86, 26, '_wp_attachment_backup_sizes', 'a:6:{s:15:"resized-700x300";a:5:{s:4:"path";s:79:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/AFP-HPB_1400x438-700x300.jpg";s:4:"file";s:28:"AFP-HPB_1400x438-700x300.jpg";s:5:"width";i:700;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-620x266";a:5:{s:4:"path";s:79:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/AFP-HPB_1400x438-620x266.jpg";s:4:"file";s:28:"AFP-HPB_1400x438-620x266.jpg";s:5:"width";i:620;s:6:"height";i:266;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-400x171";a:5:{s:4:"path";s:79:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/AFP-HPB_1400x438-400x171.jpg";s:4:"file";s:28:"AFP-HPB_1400x438-400x171.jpg";s:5:"width";i:400;s:6:"height";i:171;s:9:"mime-type";s:10:"image/jpeg";}s:16:"resized-1051x438";a:5:{s:4:"path";s:80:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/AFP-HPB_1400x438-1051x438.jpg";s:4:"file";s:29:"AFP-HPB_1400x438-1051x438.jpg";s:5:"width";i:1051;s:6:"height";i:438;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-620x258";a:5:{s:4:"path";s:79:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/AFP-HPB_1400x438-620x258.jpg";s:4:"file";s:28:"AFP-HPB_1400x438-620x258.jpg";s:5:"width";i:620;s:6:"height";i:258;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-400x167";a:5:{s:4:"path";s:79:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/AFP-HPB_1400x438-400x167.jpg";s:4:"file";s:28:"AFP-HPB_1400x438-400x167.jpg";s:5:"width";i:400;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}}'),
(87, 27, '_wp_attachment_backup_sizes', 'a:6:{s:15:"resized-700x300";a:5:{s:4:"path";s:83:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/Nemo-HPB_1400x438_Q3-700x300.jpg";s:4:"file";s:32:"Nemo-HPB_1400x438_Q3-700x300.jpg";s:5:"width";i:700;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-620x266";a:5:{s:4:"path";s:83:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/Nemo-HPB_1400x438_Q3-620x266.jpg";s:4:"file";s:32:"Nemo-HPB_1400x438_Q3-620x266.jpg";s:5:"width";i:620;s:6:"height";i:266;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-400x171";a:5:{s:4:"path";s:83:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/Nemo-HPB_1400x438_Q3-400x171.jpg";s:4:"file";s:32:"Nemo-HPB_1400x438_Q3-400x171.jpg";s:5:"width";i:400;s:6:"height";i:171;s:9:"mime-type";s:10:"image/jpeg";}s:16:"resized-1051x438";a:5:{s:4:"path";s:84:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/Nemo-HPB_1400x438_Q3-1051x438.jpg";s:4:"file";s:33:"Nemo-HPB_1400x438_Q3-1051x438.jpg";s:5:"width";i:1051;s:6:"height";i:438;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-620x258";a:5:{s:4:"path";s:83:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/Nemo-HPB_1400x438_Q3-620x258.jpg";s:4:"file";s:32:"Nemo-HPB_1400x438_Q3-620x258.jpg";s:5:"width";i:620;s:6:"height";i:258;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-400x167";a:5:{s:4:"path";s:83:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/Nemo-HPB_1400x438_Q3-400x167.jpg";s:4:"file";s:32:"Nemo-HPB_1400x438_Q3-400x167.jpg";s:5:"width";i:400;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}}'),
(88, 28, 'ml-slider_crop_position', 'center-center'),
(89, 28, 'ml-slider_caption_source', 'image-caption'),
(90, 28, '_wp_attachment_image_alt', ''),
(91, 29, 'ml-slider_crop_position', 'center-center'),
(92, 29, 'ml-slider_caption_source', 'image-caption'),
(93, 29, '_wp_attachment_image_alt', ''),
(94, 30, 'ml-slider_crop_position', 'center-center'),
(95, 30, 'ml-slider_caption_source', 'image-caption'),
(96, 30, '_wp_attachment_image_alt', ''),
(97, 31, '_wp_attached_file', '2019/09/slide2-2.jpg'),
(98, 31, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1920;s:6:"height";i:800;s:4:"file";s:20:"2019/09/slide2-2.jpg";s:5:"sizes";a:8:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"slide2-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"slide2-2-300x125.jpg";s:5:"width";i:300;s:6:"height";i:125;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:20:"slide2-2-768x320.jpg";s:5:"width";i:768;s:6:"height";i:320;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"slide2-2-1024x427.jpg";s:5:"width";i:1024;s:6:"height";i:427;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:21:"slide2-2-1568x653.jpg";s:5:"width";i:1568;s:6:"height";i:653;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-700x300";a:4:{s:4:"file";s:20:"slide2-2-700x300.jpg";s:5:"width";i:700;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-620x266";a:4:{s:4:"file";s:20:"slide2-2-620x266.jpg";s:5:"width";i:620;s:6:"height";i:266;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-400x171";a:4:{s:4:"file";s:20:"slide2-2-400x171.jpg";s:5:"width";i:400;s:6:"height";i:171;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(99, 32, '_thumbnail_id', '31'),
(100, 32, 'ml-slider_type', 'image'),
(101, 32, 'ml-slider_inherit_image_title', '1'),
(102, 32, 'ml-slider_inherit_image_alt', '1'),
(103, 32, 'ml-slider_crop_position', 'center-center'),
(104, 32, 'ml-slider_caption_source', 'image-caption'),
(105, 32, '_wp_attachment_image_alt', ''),
(106, 31, '_wp_attachment_backup_sizes', 'a:3:{s:15:"resized-700x300";a:5:{s:4:"path";s:71:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/slide2-2-700x300.jpg";s:4:"file";s:20:"slide2-2-700x300.jpg";s:5:"width";i:700;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-620x266";a:5:{s:4:"path";s:71:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/slide2-2-620x266.jpg";s:4:"file";s:20:"slide2-2-620x266.jpg";s:5:"width";i:620;s:6:"height";i:266;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-400x171";a:5:{s:4:"path";s:71:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/slide2-2-400x171.jpg";s:4:"file";s:20:"slide2-2-400x171.jpg";s:5:"width";i:400;s:6:"height";i:171;s:9:"mime-type";s:10:"image/jpeg";}}'),
(107, 34, '_wp_attached_file', '2019/09/slide2-2-1.jpg'),
(108, 34, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1920;s:6:"height";i:800;s:4:"file";s:22:"2019/09/slide2-2-1.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"slide2-2-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"slide2-2-1-300x125.jpg";s:5:"width";i:300;s:6:"height";i:125;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:22:"slide2-2-1-768x320.jpg";s:5:"width";i:768;s:6:"height";i:320;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:23:"slide2-2-1-1024x427.jpg";s:5:"width";i:1024;s:6:"height";i:427;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:23:"slide2-2-1-1568x653.jpg";s:5:"width";i:1568;s:6:"height";i:653;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(109, 35, '_thumbnail_id', '31'),
(110, 35, 'ml-slider_type', 'image'),
(111, 35, 'ml-slider_inherit_image_title', '1'),
(112, 35, 'ml-slider_inherit_image_alt', '1'),
(113, 35, 'ml-slider_crop_position', 'center-center'),
(114, 35, 'ml-slider_caption_source', 'image-caption'),
(115, 35, '_wp_attachment_image_alt', ''),
(116, 37, '_edit_lock', '1569238238:1'),
(117, 37, '_wp_trash_meta_status', 'publish'),
(118, 37, '_wp_trash_meta_time', '1569238240'),
(120, 12, 'gf_format_gallery_images', ''),
(121, 12, '_yoast_wpseo_focuskw', 'fundisa-home'),
(122, 12, '_yoast_wpseo_linkdex', '15'),
(123, 12, '_yoast_wpseo_content_score', '30'),
(124, 12, '_yoast_wpseo_is_cornerstone', '1'),
(125, 46, '_wp_attached_file', '2019/09/photodune-2076176-herd-of-elephants-head-on-l-540x330.jpg'),
(126, 46, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:540;s:6:"height";i:330;s:4:"file";s:65:"2019/09/photodune-2076176-herd-of-elephants-head-on-l-540x330.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:65:"photodune-2076176-herd-of-elephants-head-on-l-540x330-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:65:"photodune-2076176-herd-of-elephants-head-on-l-540x330-300x183.jpg";s:5:"width";i:300;s:6:"height";i:183;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(127, 1, '_edit_lock', '1569240164:1'),
(130, 1, '_thumbnail_id', '46'),
(131, 1, '_edit_last', '1'),
(134, 1, '_yoast_wpseo_primary_category', ''),
(135, 1, '_yoast_wpseo_content_score', '90'),
(136, 38, '_customize_restore_dismissed', '1'),
(138, 51, '_wp_trash_meta_status', 'publish'),
(139, 51, '_wp_trash_meta_time', '1569243280'),
(140, 50, '_customize_restore_dismissed', '1'),
(142, 52, '_customize_restore_dismissed', '1'),
(143, 54, '_wp_trash_meta_status', 'publish'),
(144, 54, '_wp_trash_meta_time', '1569243766'),
(145, 55, '_wp_trash_meta_status', 'publish'),
(146, 55, '_wp_trash_meta_time', '1569243816'),
(147, 58, '_wp_attached_file', '2019/09/cropped-slide2-2.jpg'),
(148, 58, '_wp_attachment_context', 'custom-header'),
(149, 58, '_wp_attachment_metadata', 'a:6:{s:5:"width";i:1920;s:6:"height";i:800;s:4:"file";s:28:"2019/09/cropped-slide2-2.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:28:"cropped-slide2-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:28:"cropped-slide2-2-300x125.jpg";s:5:"width";i:300;s:6:"height";i:125;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:28:"cropped-slide2-2-768x320.jpg";s:5:"width";i:768;s:6:"height";i:320;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:29:"cropped-slide2-2-1024x427.jpg";s:5:"width";i:1024;s:6:"height";i:427;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:28:"cropped-slide2-2-700x500.jpg";s:5:"width";i:700;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}s:17:"attachment_parent";i:31;}'),
(150, 58, '_wp_attachment_custom_header_last_used_shark-business', '1569244015'),
(151, 58, '_wp_attachment_is_custom_header', 'shark-business'),
(152, 59, '_wp_trash_meta_status', 'publish'),
(153, 59, '_wp_trash_meta_time', '1569244015'),
(154, 60, '_wp_trash_meta_status', 'publish'),
(155, 60, '_wp_trash_meta_time', '1569244023'),
(156, 61, '_wp_trash_meta_status', 'publish'),
(157, 61, '_wp_trash_meta_time', '1569244054'),
(159, 12, '_wp_page_template', 'homepage.php'),
(160, 10, 'gf_format_gallery_images', ''),
(161, 10, '_yoast_wpseo_content_score', '60'),
(162, 10, 'metaslider_slideshow_theme', ''),
(163, 64, '_edit_lock', '1569245412:1'),
(166, 64, '_thumbnail_id', '46'),
(167, 64, '_edit_last', '1'),
(170, 64, '_yoast_wpseo_primary_category', ''),
(171, 64, '_yoast_wpseo_content_score', '60'),
(172, 66, '_edit_lock', '1569245501:1'),
(173, 67, '_wp_attached_file', '2019/09/cq5dam.thumbnail.294.216.iph_.grid_.png.img_.jpeg'),
(174, 67, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:294;s:6:"height";i:216;s:4:"file";s:57:"2019/09/cq5dam.thumbnail.294.216.iph_.grid_.png.img_.jpeg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:57:"cq5dam.thumbnail.294.216.iph_.grid_.png.img_-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(177, 66, '_thumbnail_id', '67'),
(178, 66, '_edit_last', '1'),
(181, 66, '_yoast_wpseo_primary_category', ''),
(182, 66, '_yoast_wpseo_content_score', '60'),
(183, 69, '_edit_lock', '1569245561:1'),
(186, 69, '_thumbnail_id', '46'),
(187, 69, '_edit_last', '1'),
(188, 69, '_pingme', '1'),
(189, 69, '_encloseme', '1'),
(190, 69, '_yoast_wpseo_primary_category', ''),
(191, 69, '_yoast_wpseo_content_score', '30'),
(193, 71, '_customize_changeset_uuid', '45928a7b-b6c5-4fb7-846f-ae267d075ec6'),
(194, 72, '_edit_lock', '1569245744:1'),
(196, 73, '_customize_changeset_uuid', '45928a7b-b6c5-4fb7-846f-ae267d075ec6'),
(198, 74, '_customize_changeset_uuid', '45928a7b-b6c5-4fb7-846f-ae267d075ec6'),
(199, 72, '_wp_trash_meta_status', 'publish'),
(200, 72, '_wp_trash_meta_time', '1569245762'),
(202, 78, '_customize_restore_dismissed', '1'),
(203, 71, '_edit_lock', '1569245867:1'),
(204, 71, '_edit_last', '1'),
(205, 71, 'gf_format_video_embed', ''),
(206, 71, 'gf_format_gallery_images', ''),
(207, 71, '_yoast_wpseo_title', '%%title%% %%page%% %%sep%% %%sitename%% % %'),
(208, 71, '_yoast_wpseo_metadesc', '%'),
(209, 71, '_yoast_wpseo_content_score', '30'),
(210, 71, '_yoast_wpseo_is_cornerstone', '1'),
(211, 35, '_wp_desired_post_slug', 'slider-23-image-5'),
(212, 80, '_wp_attached_file', '2019/09/image00001.jpeg'),
(213, 80, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2448;s:6:"height";i:3264;s:4:"file";s:23:"2019/09/image00001.jpeg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"image00001-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"image00001-225x300.jpeg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:24:"image00001-768x1024.jpeg";s:5:"width";i:768;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:24:"image00001-768x1024.jpeg";s:5:"width";i:768;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:23:"image00001-700x500.jpeg";s:5:"width";i:700;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:8:"iPhone 6";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1558785539";s:9:"copyright";s:0:"";s:12:"focal_length";s:4:"4.15";s:3:"iso";s:2:"32";s:13:"shutter_speed";s:18:"0.0017452006980803";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(214, 81, '_wp_attached_file', '2019/09/image00002.jpeg'),
(215, 81, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2448;s:6:"height";i:3264;s:4:"file";s:23:"2019/09/image00002.jpeg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"image00002-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"image00002-225x300.jpeg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:24:"image00002-768x1024.jpeg";s:5:"width";i:768;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:24:"image00002-768x1024.jpeg";s:5:"width";i:768;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:23:"image00002-700x500.jpeg";s:5:"width";i:700;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:8:"iPhone 6";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1558786038";s:9:"copyright";s:0:"";s:12:"focal_length";s:4:"4.15";s:3:"iso";s:3:"400";s:13:"shutter_speed";s:17:"0.058823529411765";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(216, 82, '_wp_attached_file', '2019/09/image00003.jpeg'),
(217, 82, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2448;s:6:"height";i:3264;s:4:"file";s:23:"2019/09/image00003.jpeg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"image00003-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"image00003-225x300.jpeg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:24:"image00003-768x1024.jpeg";s:5:"width";i:768;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:24:"image00003-768x1024.jpeg";s:5:"width";i:768;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:23:"image00003-700x500.jpeg";s:5:"width";i:700;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}s:28:"meta-slider-resized-1920x800";a:4:{s:4:"file";s:24:"image00003-1920x800.jpeg";s:5:"width";i:1920;s:6:"height";i:800;s:9:"mime-type";s:10:"image/jpeg";}s:28:"meta-slider-resized-1600x667";a:4:{s:4:"file";s:24:"image00003-1600x667.jpeg";s:5:"width";i:1600;s:6:"height";i:667;s:9:"mime-type";s:10:"image/jpeg";}s:28:"meta-slider-resized-1200x500";a:4:{s:4:"file";s:24:"image00003-1200x500.jpeg";s:5:"width";i:1200;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-620x258";a:4:{s:4:"file";s:23:"image00003-620x258.jpeg";s:5:"width";i:620;s:6:"height";i:258;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-400x167";a:4:{s:4:"file";s:23:"image00003-400x167.jpeg";s:5:"width";i:400;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:8:"iPhone 6";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1558786408";s:9:"copyright";s:0:"";s:12:"focal_length";s:4:"4.15";s:3:"iso";s:3:"160";s:13:"shutter_speed";s:16:"0.03030303030303";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(218, 83, '_wp_attached_file', '2019/09/image00004.jpeg'),
(219, 83, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2873;s:6:"height";i:2448;s:4:"file";s:23:"2019/09/image00004.jpeg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"image00004-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"image00004-300x256.jpeg";s:5:"width";i:300;s:6:"height";i:256;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:23:"image00004-768x654.jpeg";s:5:"width";i:768;s:6:"height";i:654;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:24:"image00004-1024x873.jpeg";s:5:"width";i:1024;s:6:"height";i:873;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:23:"image00004-700x500.jpeg";s:5:"width";i:700;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:8:"iPhone 6";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1558786574";s:9:"copyright";s:0:"";s:12:"focal_length";s:4:"4.15";s:3:"iso";s:3:"125";s:13:"shutter_speed";s:16:"0.03030303030303";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(220, 84, '_wp_attached_file', '2019/09/image00005.jpeg'),
(221, 84, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2448;s:6:"height";i:3264;s:4:"file";s:23:"2019/09/image00005.jpeg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"image00005-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"image00005-225x300.jpeg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:24:"image00005-768x1024.jpeg";s:5:"width";i:768;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:24:"image00005-768x1024.jpeg";s:5:"width";i:768;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:23:"image00005-700x500.jpeg";s:5:"width";i:700;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:8:"iPhone 6";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1558786895";s:9:"copyright";s:0:"";s:12:"focal_length";s:4:"4.15";s:3:"iso";s:3:"160";s:13:"shutter_speed";s:16:"0.03030303030303";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(222, 85, '_wp_attached_file', '2019/09/image00006.jpeg'),
(223, 85, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3264;s:6:"height";i:2448;s:4:"file";s:23:"2019/09/image00006.jpeg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"image00006-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"image00006-300x225.jpeg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:23:"image00006-768x576.jpeg";s:5:"width";i:768;s:6:"height";i:576;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:24:"image00006-1024x768.jpeg";s:5:"width";i:1024;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:23:"image00006-700x500.jpeg";s:5:"width";i:700;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:8:"iPhone 6";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1558787332";s:9:"copyright";s:0:"";s:12:"focal_length";s:4:"4.15";s:3:"iso";s:2:"40";s:13:"shutter_speed";s:16:"0.03030303030303";s:5:"title";s:0:"";s:11:"orientation";s:1:"6";s:8:"keywords";a:0:{}}}'),
(224, 86, '_wp_attached_file', '2019/09/image00007.jpeg'),
(225, 86, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3264;s:6:"height";i:2448;s:4:"file";s:23:"2019/09/image00007.jpeg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"image00007-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"image00007-300x225.jpeg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:23:"image00007-768x576.jpeg";s:5:"width";i:768;s:6:"height";i:576;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:24:"image00007-1024x768.jpeg";s:5:"width";i:1024;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:23:"image00007-700x500.jpeg";s:5:"width";i:700;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}s:28:"meta-slider-resized-1920x800";a:4:{s:4:"file";s:24:"image00007-1920x800.jpeg";s:5:"width";i:1920;s:6:"height";i:800;s:9:"mime-type";s:10:"image/jpeg";}s:28:"meta-slider-resized-1600x667";a:4:{s:4:"file";s:24:"image00007-1600x667.jpeg";s:5:"width";i:1600;s:6:"height";i:667;s:9:"mime-type";s:10:"image/jpeg";}s:28:"meta-slider-resized-1200x500";a:4:{s:4:"file";s:24:"image00007-1200x500.jpeg";s:5:"width";i:1200;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-620x258";a:4:{s:4:"file";s:23:"image00007-620x258.jpeg";s:5:"width";i:620;s:6:"height";i:258;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-400x167";a:4:{s:4:"file";s:23:"image00007-400x167.jpeg";s:5:"width";i:400;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:8:"iPhone 6";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1559320635";s:9:"copyright";s:0:"";s:12:"focal_length";s:4:"4.15";s:3:"iso";s:2:"32";s:13:"shutter_speed";s:19:"0.00068306010928962";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(226, 87, '_wp_attached_file', '2019/09/image00008.jpeg'),
(227, 87, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2352;s:6:"height";i:2352;s:4:"file";s:23:"2019/09/image00008.jpeg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"image00008-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"image00008-300x300.jpeg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:23:"image00008-768x768.jpeg";s:5:"width";i:768;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:25:"image00008-1024x1024.jpeg";s:5:"width";i:1024;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:23:"image00008-700x500.jpeg";s:5:"width";i:700;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:8:"iPhone 6";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1559922863";s:9:"copyright";s:0:"";s:12:"focal_length";s:4:"4.15";s:3:"iso";s:3:"250";s:13:"shutter_speed";s:16:"0.03030303030303";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(228, 88, '_wp_attached_file', '2019/09/image00009.jpeg'),
(229, 88, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2448;s:6:"height";i:3264;s:4:"file";s:23:"2019/09/image00009.jpeg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"image00009-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"image00009-225x300.jpeg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:24:"image00009-768x1024.jpeg";s:5:"width";i:768;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:24:"image00009-768x1024.jpeg";s:5:"width";i:768;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:23:"image00009-700x500.jpeg";s:5:"width";i:700;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:8:"iPhone 6";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1561470562";s:9:"copyright";s:0:"";s:12:"focal_length";s:4:"4.15";s:3:"iso";s:2:"80";s:13:"shutter_speed";s:16:"0.03030303030303";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(230, 89, '_wp_attached_file', '2019/09/image00010.jpeg'),
(231, 89, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2448;s:6:"height";i:3264;s:4:"file";s:23:"2019/09/image00010.jpeg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"image00010-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"image00010-225x300.jpeg";s:5:"width";i:225;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:24:"image00010-768x1024.jpeg";s:5:"width";i:768;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:24:"image00010-768x1024.jpeg";s:5:"width";i:768;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:23:"image00010-700x500.jpeg";s:5:"width";i:700;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:8:"iPhone 6";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1561725656";s:9:"copyright";s:0:"";s:12:"focal_length";s:4:"4.15";s:3:"iso";s:3:"200";s:13:"shutter_speed";s:16:"0.03030303030303";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(232, 90, '_wp_attached_file', '2019/09/image00011.jpeg'),
(233, 90, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2183;s:6:"height";i:2183;s:4:"file";s:23:"2019/09/image00011.jpeg";s:5:"sizes";a:10:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"image00011-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:23:"image00011-300x300.jpeg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:23:"image00011-768x768.jpeg";s:5:"width";i:768;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:25:"image00011-1024x1024.jpeg";s:5:"width";i:1024;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:23:"image00011-700x500.jpeg";s:5:"width";i:700;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}s:28:"meta-slider-resized-1920x800";a:4:{s:4:"file";s:24:"image00011-1920x800.jpeg";s:5:"width";i:1920;s:6:"height";i:800;s:9:"mime-type";s:10:"image/jpeg";}s:28:"meta-slider-resized-1600x667";a:4:{s:4:"file";s:24:"image00011-1600x667.jpeg";s:5:"width";i:1600;s:6:"height";i:667;s:9:"mime-type";s:10:"image/jpeg";}s:28:"meta-slider-resized-1200x500";a:4:{s:4:"file";s:24:"image00011-1200x500.jpeg";s:5:"width";i:1200;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-620x258";a:4:{s:4:"file";s:23:"image00011-620x258.jpeg";s:5:"width";i:620;s:6:"height";i:258;s:9:"mime-type";s:10:"image/jpeg";}s:27:"meta-slider-resized-400x167";a:4:{s:4:"file";s:23:"image00011-400x167.jpeg";s:5:"width";i:400;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:3:"2.2";s:6:"credit";s:0:"";s:6:"camera";s:8:"iPhone 6";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1558786203";s:9:"copyright";s:0:"";s:12:"focal_length";s:4:"4.15";s:3:"iso";s:3:"250";s:13:"shutter_speed";s:16:"0.03030303030303";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(234, 86, '_wp_attachment_backup_sizes', 'a:5:{s:16:"resized-1920x800";a:5:{s:4:"path";s:75:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/image00007-1920x800.jpeg";s:4:"file";s:24:"image00007-1920x800.jpeg";s:5:"width";i:1920;s:6:"height";i:800;s:9:"mime-type";s:10:"image/jpeg";}s:16:"resized-1600x667";a:5:{s:4:"path";s:75:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/image00007-1600x667.jpeg";s:4:"file";s:24:"image00007-1600x667.jpeg";s:5:"width";i:1600;s:6:"height";i:667;s:9:"mime-type";s:10:"image/jpeg";}s:16:"resized-1200x500";a:5:{s:4:"path";s:75:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/image00007-1200x500.jpeg";s:4:"file";s:24:"image00007-1200x500.jpeg";s:5:"width";i:1200;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-620x258";a:5:{s:4:"path";s:74:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/image00007-620x258.jpeg";s:4:"file";s:23:"image00007-620x258.jpeg";s:5:"width";i:620;s:6:"height";i:258;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-400x167";a:5:{s:4:"path";s:74:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/image00007-400x167.jpeg";s:4:"file";s:23:"image00007-400x167.jpeg";s:5:"width";i:400;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}}'),
(235, 90, '_wp_attachment_backup_sizes', 'a:5:{s:16:"resized-1920x800";a:5:{s:4:"path";s:75:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/image00011-1920x800.jpeg";s:4:"file";s:24:"image00011-1920x800.jpeg";s:5:"width";i:1920;s:6:"height";i:800;s:9:"mime-type";s:10:"image/jpeg";}s:16:"resized-1600x667";a:5:{s:4:"path";s:75:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/image00011-1600x667.jpeg";s:4:"file";s:24:"image00011-1600x667.jpeg";s:5:"width";i:1600;s:6:"height";i:667;s:9:"mime-type";s:10:"image/jpeg";}s:16:"resized-1200x500";a:5:{s:4:"path";s:75:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/image00011-1200x500.jpeg";s:4:"file";s:24:"image00011-1200x500.jpeg";s:5:"width";i:1200;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-620x258";a:5:{s:4:"path";s:74:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/image00011-620x258.jpeg";s:4:"file";s:23:"image00011-620x258.jpeg";s:5:"width";i:620;s:6:"height";i:258;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-400x167";a:5:{s:4:"path";s:74:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/image00011-400x167.jpeg";s:4:"file";s:23:"image00011-400x167.jpeg";s:5:"width";i:400;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}}'),
(236, 82, '_wp_attachment_backup_sizes', 'a:5:{s:16:"resized-1920x800";a:5:{s:4:"path";s:75:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/image00003-1920x800.jpeg";s:4:"file";s:24:"image00003-1920x800.jpeg";s:5:"width";i:1920;s:6:"height";i:800;s:9:"mime-type";s:10:"image/jpeg";}s:16:"resized-1600x667";a:5:{s:4:"path";s:75:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/image00003-1600x667.jpeg";s:4:"file";s:24:"image00003-1600x667.jpeg";s:5:"width";i:1600;s:6:"height";i:667;s:9:"mime-type";s:10:"image/jpeg";}s:16:"resized-1200x500";a:5:{s:4:"path";s:75:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/image00003-1200x500.jpeg";s:4:"file";s:24:"image00003-1200x500.jpeg";s:5:"width";i:1200;s:6:"height";i:500;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-620x258";a:5:{s:4:"path";s:74:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/image00003-620x258.jpeg";s:4:"file";s:23:"image00003-620x258.jpeg";s:5:"width";i:620;s:6:"height";i:258;s:9:"mime-type";s:10:"image/jpeg";}s:15:"resized-400x167";a:5:{s:4:"path";s:74:"C:Amppswwwprofile.local/wp-content/uploads/2019/09/image00003-400x167.jpeg";s:4:"file";s:23:"image00003-400x167.jpeg";s:5:"width";i:400;s:6:"height";i:167;s:9:"mime-type";s:10:"image/jpeg";}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE IF NOT EXISTS `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-09-17 06:10:00', '2019-09-17 06:10:00', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2019-09-23 12:02:43', '2019-09-23 12:02:43', '', 0, 'http://localhost/profile.local/?p=1', 0, 'post', '', 1),
(2, 1, '2019-09-17 06:10:00', '2019-09-17 06:10:00', '<!-- wp:paragraph -->\n<p>This is an example page. It''s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>Hi there! I''m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin'' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href="http://localhost/profile.local/wp-admin/">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2019-09-17 06:10:00', '2019-09-17 06:10:00', '', 0, 'http://localhost/profile.local/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-09-17 06:10:00', '2019-09-17 06:10:00', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost/profile.local.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {"level":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {"level":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {"level":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {"level":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {"level":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2019-09-17 06:10:00', '2019-09-17 06:10:00', '', 0, 'http://localhost/profile.local/?page_id=3', 0, 'page', '', 0),
(5, 1, '2019-09-23 10:03:14', '0000-00-00 00:00:00', '', 'Home', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-09-23 10:03:14', '0000-00-00 00:00:00', '', 0, 'http://localhost/profile.local/?p=5', 1, 'nav_menu_item', '', 0),
(6, 1, '2019-09-23 10:05:08', '2019-09-23 10:05:08', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2019-09-23 10:45:14', '2019-09-23 10:45:14', '', 0, 'http://localhost/profile.local/?p=6', 1, 'nav_menu_item', '', 0),
(7, 1, '2019-09-23 10:05:08', '2019-09-23 10:05:08', '', 'About', '', 'publish', 'closed', 'closed', '', 'about', '', '', '2019-09-23 10:45:14', '2019-09-23 10:45:14', '', 0, 'http://localhost/profile.local/?p=7', 2, 'nav_menu_item', '', 0),
(8, 1, '2019-09-23 10:05:08', '2019-09-23 10:05:08', '', 'Contact', '', 'publish', 'closed', 'closed', '', 'contact', '', '', '2019-09-23 10:45:14', '2019-09-23 10:45:14', '', 0, 'http://localhost/profile.local/?p=8', 4, 'nav_menu_item', '', 0),
(9, 1, '2019-09-23 10:05:08', '2019-09-23 10:05:08', '', 'Services', '', 'publish', 'closed', 'closed', '', 'services', '', '', '2019-09-23 10:45:14', '2019-09-23 10:45:14', '', 0, 'http://localhost/profile.local/?p=9', 3, 'nav_menu_item', '', 0),
(10, 1, '2019-09-23 10:06:57', '2019-09-23 10:06:57', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'About', '', 'publish', 'closed', 'closed', '', 'about', '', '', '2019-09-23 13:28:22', '2019-09-23 13:28:22', '', 0, 'http://localhost/profile.local/?page_id=10', 0, 'page', '', 0),
(11, 1, '2019-09-23 10:06:57', '2019-09-23 10:06:57', '', 'About', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2019-09-23 10:06:57', '2019-09-23 10:06:57', '', 10, 'http://localhost/profile.local/2019/09/23/10-revision-v1/', 0, 'revision', '', 0),
(12, 1, '2019-09-23 10:08:58', '2019-09-23 10:08:58', '<!-- wp:metaslider/slider {"slideshowId":23,"stretch":"wide"} -->\n<div class="alignwide">[metaslider id=23 cssclass=""]</div>\n<!-- /wp:metaslider/slider -->\n\n<!-- wp:columns {"columns":3} -->\n<div class="wp-block-columns has-3-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns {"columns":3,"align":"wide"} -->\n<div class="wp-block-columns alignwide has-3-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted -->\n\n<!-- wp:more -->\n<!--more-->\n<!-- /wp:more --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Home', '', 'draft', 'closed', 'closed', '', 'home-page', '', '', '2019-09-23 13:40:32', '2019-09-23 13:40:32', '', 0, 'http://localhost/profile.local/?page_id=12', 0, 'page', '', 0),
(13, 1, '2019-09-23 10:08:58', '2019-09-23 10:08:58', '', 'Home', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2019-09-23 10:08:58', '2019-09-23 10:08:58', '', 12, 'http://localhost/profile.local/2019/09/23/12-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2019-09-23 10:09:55', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-23 10:09:55', '0000-00-00 00:00:00', '', 0, 'http://localhost/profile.local/?page_id=14', 0, 'page', '', 0),
(15, 1, '2019-09-23 10:13:08', '2019-09-23 10:13:08', '{"id":"15","field_id":3,"fields":[{"id":"0","type":"name","label":"Name","format":"first-last","description":"","required":"1","size":"medium","simple_placeholder":"","simple_default":"","first_placeholder":"","first_default":"","middle_placeholder":"","middle_default":"","last_placeholder":"","last_default":"","css":""},{"id":"1","type":"email","label":"Email","description":"","required":"1","size":"medium","placeholder":"","confirmation_placeholder":"","default_value":"","css":""},{"id":"2","type":"textarea","label":"Comment or Message","description":"","required":"1","size":"medium","placeholder":"","css":""}],"settings":{"form_title":"Simple Contact Form","form_desc":"","form_class":"","submit_text":"Submit","submit_text_processing":"Sending...","submit_class":"","honeypot":"1","notification_enable":"1","notifications":{"1":{"email":"{admin_email}","subject":"New Entry: Simple Contact Form","sender_name":"Fundisa","sender_address":"{admin_email}","replyto":"{field_id=\\"1\\"}","message":"{all_fields}"}},"confirmations":{"1":{"type":"message","message":"<p>Thanks for contacting us! We will be in touch with you shortly.<\\/p>","message_scroll":"1","page":"10","redirect":""}}},"meta":{"template":"contact"}}', 'Simple Contact Form', '', 'publish', 'closed', 'closed', '', 'simple-contact-form', '', '', '2019-09-23 10:13:33', '2019-09-23 10:13:33', '', 0, 'http://localhost/profile.local/?post_type=wpforms&#038;p=15', 0, 'wpforms', '', 0),
(16, 1, '2019-09-23 10:14:15', '2019-09-23 10:14:15', '<!-- wp:shortcode -->\n[wpforms id="15" title="false" description="false"]\n<!-- /wp:shortcode -->', 'Contact', '', 'publish', 'closed', 'closed', '', 'contact', '', '', '2019-09-23 10:14:15', '2019-09-23 10:14:15', '', 0, 'http://localhost/profile.local/?page_id=16', 0, 'page', '', 0),
(17, 1, '2019-09-23 10:14:15', '2019-09-23 10:14:15', '<!-- wp:shortcode -->\n[wpforms id="15" title="false" description="false"]\n<!-- /wp:shortcode -->', 'Contact', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2019-09-23 10:14:15', '2019-09-23 10:14:15', '', 16, 'http://localhost/profile.local/2019/09/23/16-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2019-09-23 10:14:48', '2019-09-23 10:14:48', '{\n    "blogdescription": {\n        "value": "",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 10:14:48"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'ac8b08e9-1750-41af-8350-f82db372673e', '', '', '2019-09-23 10:14:48', '2019-09-23 10:14:48', '', 0, 'http://localhost/profile.local/2019/09/23/ac8b08e9-1750-41af-8350-f82db372673e/', 0, 'customize_changeset', '', 0),
(19, 1, '2019-09-23 10:14:57', '2019-09-23 10:14:57', '{\n    "twentynineteen::primary_color": {\n        "value": "default",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 10:14:57"\n    },\n    "twentynineteen::primary_color_hue": {\n        "value": 206,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 10:14:57"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '4ccd9553-5014-42d6-8ec9-2146bbcb745a', '', '', '2019-09-23 10:14:57', '2019-09-23 10:14:57', '', 0, 'http://localhost/profile.local/2019/09/23/4ccd9553-5014-42d6-8ec9-2146bbcb745a/', 0, 'customize_changeset', '', 0),
(20, 1, '2019-09-23 10:15:18', '2019-09-23 10:15:18', '{\n    "nav_menu_item[6]": {\n        "value": {\n            "menu_item_parent": 0,\n            "object_id": 6,\n            "object": "custom",\n            "type": "custom",\n            "type_label": "Custom Link",\n            "title": "Home",\n            "url": "/",\n            "target": "",\n            "attr_title": "",\n            "description": "",\n            "classes": "",\n            "xfn": "",\n            "nav_menu_term_id": 2,\n            "position": 1,\n            "status": "publish",\n            "original_title": "",\n            "_invalid": false\n        },\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 10:15:18"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '67cea003-3a0a-45ec-868f-9571b3fc5333', '', '', '2019-09-23 10:15:18', '2019-09-23 10:15:18', '', 0, 'http://localhost/profile.local/2019/09/23/67cea003-3a0a-45ec-868f-9571b3fc5333/', 0, 'customize_changeset', '', 0),
(21, 1, '2019-09-23 10:15:39', '2019-09-23 10:15:39', '{\n    "twentynineteen::nav_menu_locations[footer]": {\n        "value": 2,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 10:15:39"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '1df7bf8d-cb0f-4cf1-b447-dbfe8857c726', '', '', '2019-09-23 10:15:39', '2019-09-23 10:15:39', '', 0, 'http://localhost/profile.local/2019/09/23/1df7bf8d-cb0f-4cf1-b447-dbfe8857c726/', 0, 'customize_changeset', '', 0),
(22, 1, '2019-09-23 10:16:24', '2019-09-23 10:16:24', '{\n    "sidebars_widgets[sidebar-1]": {\n        "value": [],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 10:16:24"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'cd047ef2-292b-4881-ae46-255bca305d39', '', '', '2019-09-23 10:16:24', '2019-09-23 10:16:24', '', 0, 'http://localhost/profile.local/2019/09/23/cd047ef2-292b-4881-ae46-255bca305d39/', 0, 'customize_changeset', '', 0),
(23, 1, '2019-09-23 10:46:05', '2019-09-23 10:46:05', '', 'New Slideshow', '', 'publish', 'closed', 'closed', '', 'new-slideshow', '', '', '2019-09-27 06:24:50', '2019-09-27 06:24:50', '', 0, 'http://localhost/profile.local/?post_type=ml-slider&#038;p=23', 0, 'ml-slider', '', 0),
(25, 1, '2019-09-23 10:46:59', '2019-09-23 10:46:59', '', 'Nemo-HPB_1400x438_A1', '', 'inherit', 'open', 'closed', '', 'nemo-hpb_1400x438_a1', '', '', '2019-09-23 10:46:59', '2019-09-23 10:46:59', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/Nemo-HPB_1400x438_A1.jpg', 0, 'attachment', 'image/jpeg', 0),
(26, 1, '2019-09-23 10:46:59', '2019-09-23 10:46:59', '', 'AFP-HPB_1400x438', '', 'inherit', 'open', 'closed', '', 'afp-hpb_1400x438', '', '', '2019-09-23 10:46:59', '2019-09-23 10:46:59', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/AFP-HPB_1400x438.jpg', 0, 'attachment', 'image/jpeg', 0),
(27, 1, '2019-09-23 10:47:00', '2019-09-23 10:47:00', '', 'Nemo-HPB_1400x438_Q3', '', 'inherit', 'open', 'closed', '', 'nemo-hpb_1400x438_q3', '', '', '2019-09-23 10:47:00', '2019-09-23 10:47:00', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/Nemo-HPB_1400x438_Q3.jpg', 0, 'attachment', 'image/jpeg', 0),
(28, 1, '2019-09-23 10:47:03', '2019-09-23 10:47:03', '', 'Slider 23 - image', '', 'publish', 'closed', 'closed', '', 'slider-23-image', '', '', '2019-09-27 06:24:50', '2019-09-27 06:24:50', '', 0, 'http://localhost/profile.local/?post_type=ml-slide&#038;p=28', 2, 'ml-slide', '', 0),
(29, 1, '2019-09-23 10:47:03', '2019-09-23 10:47:03', '', 'Slider 23 - image', '', 'publish', 'closed', 'closed', '', 'slider-23-image-2', '', '', '2019-09-27 06:24:50', '2019-09-27 06:24:50', '', 0, 'http://localhost/profile.local/?post_type=ml-slide&#038;p=29', 3, 'ml-slide', '', 0),
(30, 1, '2019-09-23 10:47:03', '2019-09-23 10:47:03', '', 'Slider 23 - image', '', 'publish', 'closed', 'closed', '', 'slider-23-image-3', '', '', '2019-09-27 06:24:50', '2019-09-27 06:24:50', '', 0, 'http://localhost/profile.local/?post_type=ml-slide&#038;p=30', 4, 'ml-slide', '', 0),
(31, 1, '2019-09-23 10:49:16', '2019-09-23 10:49:16', '', 'slide2-2', '', 'inherit', 'open', 'closed', '', 'slide2-2', '', '', '2019-09-23 10:49:16', '2019-09-23 10:49:16', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/slide2-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(32, 1, '2019-09-23 10:49:19', '2019-09-23 10:49:19', '', 'Slider 23 - image', '', 'publish', 'closed', 'closed', '', 'slider-23-image-4', '', '', '2019-09-27 06:24:50', '2019-09-27 06:24:50', '', 0, 'http://localhost/profile.local/?post_type=ml-slide&#038;p=32', 0, 'ml-slide', '', 0),
(33, 1, '2019-09-23 10:50:21', '2019-09-23 10:50:21', '<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:metaslider/slider {"slideshowId":23} -->\n<div class="alignnormal">[metaslider id=23 cssclass=""]</div>\n<!-- /wp:metaslider/slider -->', 'Home', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2019-09-23 10:50:21', '2019-09-23 10:50:21', '', 12, 'http://localhost/profile.local/2019/09/23/12-revision-v1/', 0, 'revision', '', 0),
(34, 1, '2019-09-23 10:50:32', '2019-09-23 10:50:32', '', 'slide2-2', '', 'inherit', 'open', 'closed', '', 'slide2-2-2', '', '', '2019-09-23 10:50:32', '2019-09-23 10:50:32', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/slide2-2-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(35, 1, '2019-09-23 10:50:37', '2019-09-23 10:50:37', '', 'Slider 23 - image', '', 'trash', 'closed', 'closed', '', 'slider-23-image-5__trashed', '', '', '2019-09-27 06:24:50', '2019-09-27 06:24:50', '', 0, 'http://localhost/profile.local/?post_type=ml-slide&#038;p=35', 1, 'ml-slide', '', 0),
(36, 1, '2019-09-23 10:51:47', '2019-09-23 10:51:47', '<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:metaslider/slider {"slideshowId":23,"stretch":"full"} -->\n<div class="alignfull">[metaslider id=23 cssclass=""]</div>\n<!-- /wp:metaslider/slider -->', 'Home', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2019-09-23 10:51:47', '2019-09-23 10:51:47', '', 12, 'http://localhost/profile.local/2019/09/23/12-revision-v1/', 0, 'revision', '', 0),
(37, 1, '2019-09-23 11:30:40', '2019-09-23 11:30:40', '{\n    "twentynineteen::nav_menu_locations[menu-1]": {\n        "value": 2,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 11:30:11"\n    },\n    "twentynineteen::nav_menu_locations[footer]": {\n        "value": 2,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 11:30:11"\n    },\n    "nav_menu_item[-2767284]": {\n        "value": false,\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 11:30:11"\n    },\n    "nav_menu_item[-274080987]": {\n        "value": false,\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 11:30:11"\n    },\n    "nav_menu_item[-1673052391]": {\n        "value": false,\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 11:30:11"\n    },\n    "nav_menu_item[-1559326558]": {\n        "value": false,\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 11:30:11"\n    },\n    "nav_menu_item[-428095566]": {\n        "value": false,\n        "type": "nav_menu_item",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 11:30:11"\n    },\n    "twentynineteen::primary_color": {\n        "value": "custom",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 11:30:40"\n    },\n    "twentynineteen::image_filter": {\n        "value": false,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 11:30:40"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '688368f9-9d39-45d2-8d3c-c1d8c452dd75', '', '', '2019-09-23 11:30:40', '2019-09-23 11:30:40', '', 0, 'http://localhost/profile.local/?p=37', 0, 'customize_changeset', '', 0),
(38, 1, '2019-09-23 11:31:39', '0000-00-00 00:00:00', '{\n    "twentynineteen::nav_menu_locations[menu-1]": {\n        "value": 2,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 11:31:39"\n    },\n    "twentynineteen::nav_menu_locations[footer]": {\n        "value": 2,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 11:31:10"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '6bae9acc-5344-49a2-9a2c-875a4b8f7e0f', '', '', '2019-09-23 11:31:40', '2019-09-23 11:31:40', '', 0, 'http://localhost/profile.local/?p=38', 0, 'customize_changeset', '', 0),
(39, 1, '2019-09-23 11:34:32', '2019-09-23 11:34:32', '<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:metaslider/slider {"slideshowId":23,"stretch":"full"} -->\n<div class="alignfull">[metaslider id=23 cssclass=""]</div>\n<!-- /wp:metaslider/slider -->\n\n<!-- wp:columns -->\n<div class="wp-block-columns has-2-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Home', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2019-09-23 11:34:32', '2019-09-23 11:34:32', '', 12, 'http://localhost/profile.local/2019/09/23/12-revision-v1/', 0, 'revision', '', 0),
(40, 1, '2019-09-23 11:39:39', '2019-09-23 11:39:39', '<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:metaslider/slider {"slideshowId":23,"stretch":"full"} -->\n<div class="alignfull">[metaslider id=23 cssclass=""]</div>\n<!-- /wp:metaslider/slider -->\n\n<!-- wp:columns -->\n<div class="wp-block-columns has-2-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p>\n\n[grid_plus name="New Grid"]&nbsp;\n\n</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'Home', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2019-09-23 11:39:39', '2019-09-23 11:39:39', '', 12, 'http://localhost/profile.local/2019/09/23/12-revision-v1/', 0, 'revision', '', 0),
(41, 1, '2019-09-23 11:40:07', '2019-09-23 11:40:07', '<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:metaslider/slider {"slideshowId":23,"stretch":"full"} -->\n<div class="alignfull">[metaslider id=23 cssclass=""]</div>\n<!-- /wp:metaslider/slider -->\n\n<!-- wp:columns -->\n<div class="wp-block-columns has-2-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns -->\n<div class="wp-block-columns has-2-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:shortcode -->\n[grid_plus name="New Grid"] \n<!-- /wp:shortcode -->', 'Home', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2019-09-23 11:40:07', '2019-09-23 11:40:07', '', 12, 'http://localhost/profile.local/2019/09/23/12-revision-v1/', 0, 'revision', '', 0),
(42, 1, '2019-09-23 11:41:11', '2019-09-23 11:41:11', '<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:metaslider/slider {"slideshowId":23,"stretch":"full"} -->\n<div class="alignfull">[metaslider id=23 cssclass=""]</div>\n<!-- /wp:metaslider/slider -->\n\n<!-- wp:columns -->\n<div class="wp-block-columns has-2-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns {"columns":3,"align":"wide"} -->\n<div class="wp-block-columns alignwide has-3-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p>\n\n[grid_plus name="New Grid"]&nbsp;\n\n</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p>\n\n[grid_plus name="New Grid"]&nbsp;\n\n</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p>\n\n[grid_plus name="New Grid"]&nbsp;\n\n</p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:shortcode -->\n[grid_plus name="New Grid"] \n<!-- /wp:shortcode -->', 'Home', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2019-09-23 11:41:11', '2019-09-23 11:41:11', '', 12, 'http://localhost/profile.local/2019/09/23/12-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2019-09-23 11:42:08', '2019-09-23 11:42:08', '<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:metaslider/slider {"slideshowId":23,"stretch":"full"} -->\n<div class="alignfull">[metaslider id=23 cssclass=""]</div>\n<!-- /wp:metaslider/slider -->\n\n<!-- wp:columns -->\n<div class="wp-block-columns has-2-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns {"columns":3,"align":"wide"} -->\n<div class="wp-block-columns alignwide has-3-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">\n\n[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">\n\n[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">\n\n[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Home', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2019-09-23 11:42:08', '2019-09-23 11:42:08', '', 12, 'http://localhost/profile.local/2019/09/23/12-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2019-09-23 11:44:02', '2019-09-23 11:44:02', '<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:metaslider/slider {"slideshowId":23,"stretch":"wide"} -->\n<div class="alignwide">[metaslider id=23 cssclass=""]</div>\n<!-- /wp:metaslider/slider -->\n\n<!-- wp:columns -->\n<div class="wp-block-columns has-2-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns {"columns":3,"align":"wide"} -->\n<div class="wp-block-columns alignwide has-3-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">\n[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted -->\n\n<!-- wp:more -->\n<!--more-->\n<!-- /wp:more --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">\n[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">\n[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Home', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2019-09-23 11:44:02', '2019-09-23 11:44:02', '', 12, 'http://localhost/profile.local/2019/09/23/12-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2019-09-23 11:49:48', '2019-09-23 11:49:48', '', 'photodune-2076176-herd-of-elephants-head-on-l-540x330', '', 'inherit', 'open', 'closed', '', 'photodune-2076176-herd-of-elephants-head-on-l-540x330', '', '', '2019-09-23 11:49:48', '2019-09-23 11:49:48', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/photodune-2076176-herd-of-elephants-head-on-l-540x330.jpg', 0, 'attachment', 'image/jpeg', 0),
(47, 1, '2019-09-23 12:00:53', '2019-09-23 12:00:53', '<p>home[grid_plus name="New Grid"]</p>\n\n<!-- wp:metaslider/slider {"slideshowId":23,"stretch":"wide"} -->\n<div class="alignwide">[metaslider id=23 cssclass=""]</div>\n<!-- /wp:metaslider/slider -->\n\n<!-- wp:columns -->\n<div class="wp-block-columns has-2-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns {"columns":3,"align":"wide"} -->\n<div class="wp-block-columns alignwide has-3-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted -->\n\n<!-- wp:more -->\n<!--more-->\n<!-- /wp:more --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Home', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2019-09-23 12:00:53', '2019-09-23 12:00:53', '', 12, 'http://localhost/profile.local/2019/09/23/12-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2019-09-23 12:02:41', '2019-09-23 12:02:41', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2019-09-23 12:02:41', '2019-09-23 12:02:41', '', 1, 'http://localhost/profile.local/2019/09/23/1-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2019-09-23 12:27:17', '0000-00-00 00:00:00', '{\n    "show_on_front": {\n        "value": "page",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 12:26:17"\n    },\n    "page_on_front": {\n        "value": "12",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 12:26:17"\n    },\n    "twentynineteen::primary_color": {\n        "value": "default",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 12:26:17"\n    },\n    "page_for_posts": {\n        "value": "2",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 12:27:17"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '00167d31-7b93-4886-955f-494a84273e93', '', '', '2019-09-23 12:27:17', '2019-09-23 12:27:17', '', 0, 'http://localhost/profile.local/?p=50', 0, 'customize_changeset', '', 0),
(51, 1, '2019-09-23 12:54:40', '2019-09-23 12:54:40', '{\n    "old_sidebars_widgets_data": {\n        "value": {\n            "wp_inactive_widgets": [],\n            "sidebar-1": []\n        },\n        "type": "global_variable",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 12:54:40"\n    },\n    "shark-business::nav_menu_locations[primary]": {\n        "value": 2,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 12:54:40"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '362dfea2-a772-4e00-8ba3-ab2ff9724d15', '', '', '2019-09-23 12:54:40', '2019-09-23 12:54:40', '', 0, 'http://localhost/profile.local/2019/09/23/362dfea2-a772-4e00-8ba3-ab2ff9724d15/', 0, 'customize_changeset', '', 0),
(52, 1, '2019-09-23 12:58:02', '0000-00-00 00:00:00', '{\n    "shark-business::header_textcolor": {\n        "value": "#ffffff",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 12:56:02"\n    },\n    "shark-business::header_image": {\n        "value": "random-default-image",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 12:56:02"\n    },\n    "shark-business::header_image_data": {\n        "value": "random-default-image",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 12:56:02"\n    },\n    "shark-business::nav_menu_locations[social]": {\n        "value": 0,\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 12:56:02"\n    },\n    "shark-business::shark_business_theme_options[enable_topbar]": {\n        "value": "false",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 12:57:02"\n    },\n    "shark-business::shark_business_theme_options[show_social_menu]": {\n        "value": "false",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 12:57:02"\n    },\n    "show_on_front": {\n        "value": "page",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 12:58:02"\n    },\n    "page_on_front": {\n        "value": "10",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 12:58:02"\n    },\n    "page_for_posts": {\n        "value": "10",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 12:58:02"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '0a070dce-a976-4bf3-a6f0-93dfbf53ba14', '', '', '2019-09-23 12:58:02', '2019-09-23 12:58:02', '', 0, 'http://localhost/profile.local/?p=52', 0, 'customize_changeset', '', 0),
(53, 1, '2019-09-23 12:59:31', '2019-09-23 12:59:31', '<!-- wp:metaslider/slider {"slideshowId":23,"stretch":"wide"} -->\n<div class="alignwide">[metaslider id=23 cssclass=""]</div>\n<!-- /wp:metaslider/slider -->', 'Untitled Reusable Block', '', 'publish', 'closed', 'closed', '', 'untitled-reusable-block', '', '', '2019-09-23 12:59:31', '2019-09-23 12:59:31', '', 0, 'http://localhost/profile.local/2019/09/23/untitled-reusable-block/', 0, 'wp_block', '', 0),
(54, 1, '2019-09-23 13:02:46', '2019-09-23 13:02:46', '{\n    "show_on_front": {\n        "value": "page",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:02:46"\n    },\n    "page_on_front": {\n        "value": "12",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:02:46"\n    },\n    "shark-business::shark_business_theme_options[slider_content_page_1]": {\n        "value": "12",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:02:46"\n    },\n    "shark-business::shark_business_theme_options[slider_content_page_2]": {\n        "value": "10",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:02:46"\n    },\n    "shark-business::shark_business_theme_options[slider_content_page_3]": {\n        "value": "16",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:02:46"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'b4b63876-278f-479c-8503-5548dfd2ebae', '', '', '2019-09-23 13:02:46', '2019-09-23 13:02:46', '', 0, 'http://localhost/profile.local/2019/09/23/b4b63876-278f-479c-8503-5548dfd2ebae/', 0, 'customize_changeset', '', 0),
(55, 1, '2019-09-23 13:03:36', '2019-09-23 13:03:36', '{\n    "shark-business::shark_business_theme_options[sidebar_single_layout]": {\n        "value": "no-sidebar",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:03:36"\n    },\n    "shark-business::shark_business_theme_options[sidebar_page_layout]": {\n        "value": "no-sidebar",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:03:36"\n    },\n    "shark-business::shark_business_theme_options[enable_sticky_header]": {\n        "value": "false",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:03:36"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '506dea5d-978d-4261-874c-42b898d64d63', '', '', '2019-09-23 13:03:36', '2019-09-23 13:03:36', '', 0, 'http://localhost/profile.local/2019/09/23/506dea5d-978d-4261-874c-42b898d64d63/', 0, 'customize_changeset', '', 0),
(56, 1, '2019-09-23 13:04:57', '2019-09-23 13:04:57', '<!-- wp:metaslider/slider {"slideshowId":23,"stretch":"wide"} -->\n<div class="alignwide">[metaslider id=23 cssclass=""]</div>\n<!-- /wp:metaslider/slider -->\n\n<!-- wp:columns -->\n<div class="wp-block-columns has-2-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns {"columns":3,"align":"wide"} -->\n<div class="wp-block-columns alignwide has-3-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted -->\n\n<!-- wp:more -->\n<!--more-->\n<!-- /wp:more --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Home', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2019-09-23 13:04:57', '2019-09-23 13:04:57', '', 12, 'http://localhost/profile.local/2019/09/23/12-revision-v1/', 0, 'revision', '', 0),
(57, 1, '2019-09-23 13:05:56', '2019-09-23 13:05:56', '<!-- wp:metaslider/slider {"slideshowId":23,"stretch":"wide"} -->\n<div class="alignwide">[metaslider id=23 cssclass=""]</div>\n<!-- /wp:metaslider/slider -->\n\n<!-- wp:columns {"columns":3} -->\n<div class="wp-block-columns has-3-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:columns {"columns":3,"align":"wide"} -->\n<div class="wp-block-columns alignwide has-3-columns"><!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted -->\n\n<!-- wp:more -->\n<!--more-->\n<!-- /wp:more --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class="wp-block-column"><!-- wp:preformatted -->\n<pre class="wp-block-preformatted">[grid_plus name="New Grid"]&nbsp;\n\n</pre>\n<!-- /wp:preformatted --></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->', 'Home', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2019-09-23 13:05:56', '2019-09-23 13:05:56', '', 12, 'http://localhost/profile.local/2019/09/23/12-revision-v1/', 0, 'revision', '', 0),
(58, 1, '2019-09-23 13:06:48', '2019-09-23 13:06:48', '', 'cropped-slide2-2.jpg', '', 'inherit', 'open', 'closed', '', 'cropped-slide2-2-jpg', '', '', '2019-09-23 13:06:48', '2019-09-23 13:06:48', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/cropped-slide2-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(59, 1, '2019-09-23 13:06:55', '2019-09-23 13:06:55', '{\n    "shark-business::header_image": {\n        "value": "http://localhost/profile.local/wp-content/uploads/2019/09/cropped-slide2-2.jpg",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:06:55"\n    },\n    "shark-business::header_image_data": {\n        "value": {\n            "url": "http://localhost/profile.local/wp-content/uploads/2019/09/cropped-slide2-2.jpg",\n            "thumbnail_url": "http://localhost/profile.local/wp-content/uploads/2019/09/cropped-slide2-2.jpg",\n            "timestamp": 1569244009192,\n            "attachment_id": 58,\n            "width": 1920,\n            "height": 800\n        },\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:06:55"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'ed428a80-4672-4ce6-a3bf-f6244ae8b2e0', '', '', '2019-09-23 13:06:55', '2019-09-23 13:06:55', '', 0, 'http://localhost/profile.local/2019/09/23/ed428a80-4672-4ce6-a3bf-f6244ae8b2e0/', 0, 'customize_changeset', '', 0),
(60, 1, '2019-09-23 13:07:03', '2019-09-23 13:07:03', '{\n    "shark-business::header_image": {\n        "value": "random-uploaded-image",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:07:03"\n    },\n    "shark-business::header_image_data": {\n        "value": "random-uploaded-image",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:07:03"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '32628a1b-6319-4eaa-8871-959b9b1c5613', '', '', '2019-09-23 13:07:03', '2019-09-23 13:07:03', '', 0, 'http://localhost/profile.local/2019/09/23/32628a1b-6319-4eaa-8871-959b9b1c5613/', 0, 'customize_changeset', '', 0),
(61, 1, '2019-09-23 13:07:34', '2019-09-23 13:07:34', '{\n    "shark-business::shark_business_theme_options[sidebar_single_layout]": {\n        "value": "right-sidebar",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:07:34"\n    },\n    "shark-business::shark_business_theme_options[sidebar_page_layout]": {\n        "value": "right-sidebar",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:07:34"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '2dbd2a41-7274-4fce-9ad1-521a9a227ba8', '', '', '2019-09-23 13:07:34', '2019-09-23 13:07:34', '', 0, 'http://localhost/profile.local/2019/09/23/2dbd2a41-7274-4fce-9ad1-521a9a227ba8/', 0, 'customize_changeset', '', 0),
(63, 1, '2019-09-23 13:27:47', '2019-09-23 13:27:47', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'About', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2019-09-23 13:27:47', '2019-09-23 13:27:47', '', 10, 'http://localhost/profile.local/2019/09/23/10-revision-v1/', 0, 'revision', '', 0),
(64, 1, '2019-09-23 13:30:09', '2019-09-23 13:30:09', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum', '', 'publish', 'open', 'open', '', 'lorem-ipsum', '', '', '2019-09-23 13:30:11', '2019-09-23 13:30:11', '', 0, 'http://localhost/profile.local/?p=64', 0, 'post', '', 0),
(65, 1, '2019-09-23 13:30:09', '2019-09-23 13:30:09', 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum', '', 'inherit', 'closed', 'closed', '', '64-revision-v1', '', '', '2019-09-23 13:30:09', '2019-09-23 13:30:09', '', 64, 'http://localhost/profile.local/2019/09/23/64-revision-v1/', 0, 'revision', '', 0),
(66, 1, '2019-09-23 13:31:38', '2019-09-23 13:31:38', 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'ipsum dolor', '', 'publish', 'open', 'open', '', 'ipsum-dolor', '', '', '2019-09-23 13:31:40', '2019-09-23 13:31:40', '', 0, 'http://localhost/profile.local/?p=66', 0, 'post', '', 0),
(67, 1, '2019-09-23 13:31:26', '2019-09-23 13:31:26', '', 'cq5dam.thumbnail.294.216.iph.grid.png.img', '', 'inherit', 'open', 'closed', '', 'cq5dam-thumbnail-294-216-iph-grid-png-img', '', '', '2019-09-23 13:31:26', '2019-09-23 13:31:26', '', 66, 'http://localhost/profile.local/wp-content/uploads/2019/09/cq5dam.thumbnail.294.216.iph_.grid_.png.img_.jpeg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(68, 1, '2019-09-23 13:31:38', '2019-09-23 13:31:38', 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'ipsum dolor', '', 'inherit', 'closed', 'closed', '', '66-revision-v1', '', '', '2019-09-23 13:31:38', '2019-09-23 13:31:38', '', 66, 'http://localhost/profile.local/2019/09/23/66-revision-v1/', 0, 'revision', '', 0),
(69, 1, '2019-09-23 13:32:38', '2019-09-23 13:32:38', 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have', 'who is thought to have', '', 'publish', 'open', 'open', '', 'who-is-thought-to-have', '', '', '2019-09-23 13:32:40', '2019-09-23 13:32:40', '', 0, 'http://localhost/profile.local/?p=69', 0, 'post', '', 0),
(70, 1, '2019-09-23 13:32:38', '2019-09-23 13:32:38', 'Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have', 'who is thought to have', '', 'inherit', 'closed', 'closed', '', '69-revision-v1', '', '', '2019-09-23 13:32:38', '2019-09-23 13:32:38', '', 69, 'http://localhost/profile.local/2019/09/23/69-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2019-09-23 13:36:02', '2019-09-23 13:36:02', '', 'Our Latest Projects', '', 'publish', 'closed', 'closed', '', 'our-latest-projects', '', '', '2019-09-23 13:39:57', '2019-09-23 13:39:57', '', 0, 'http://localhost/profile.local/?page_id=71', 0, 'page', '', 0),
(72, 1, '2019-09-23 13:36:02', '2019-09-23 13:36:02', '{\n    "page_for_posts": {\n        "value": "71",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:34:44"\n    },\n    "shark-business::shark_business_theme_options[slider_entire_site]": {\n        "value": "true",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:34:44"\n    },\n    "nav_menus_created_posts": {\n        "value": [\n            71,\n            73,\n            74\n        ],\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:35:44"\n    },\n    "show_on_front": {\n        "value": "page",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:35:44"\n    },\n    "page_on_front": {\n        "value": "12",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:35:44"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '45928a7b-b6c5-4fb7-846f-ae267d075ec6', '', '', '2019-09-23 13:36:02', '2019-09-23 13:36:02', '', 0, 'http://localhost/profile.local/?p=72', 0, 'customize_changeset', '', 0),
(73, 1, '2019-09-23 13:36:02', '2019-09-23 13:36:02', '', 'Our Latest Projects', '', 'publish', 'closed', 'closed', '', 'our-latest-projects-2', '', '', '2019-09-23 13:36:02', '2019-09-23 13:36:02', '', 0, 'http://localhost/profile.local/?page_id=73', 0, 'page', '', 0),
(74, 1, '2019-09-23 13:36:02', '2019-09-23 13:36:02', '', 'Our Latest Projects', '', 'publish', 'closed', 'closed', '', 'our-latest-projects-3', '', '', '2019-09-23 13:36:02', '2019-09-23 13:36:02', '', 0, 'http://localhost/profile.local/?page_id=74', 0, 'page', '', 0),
(75, 1, '2019-09-23 13:36:02', '2019-09-23 13:36:02', '', 'Our Latest Projects', '', 'inherit', 'closed', 'closed', '', '71-revision-v1', '', '', '2019-09-23 13:36:02', '2019-09-23 13:36:02', '', 71, 'http://localhost/profile.local/2019/09/23/71-revision-v1/', 0, 'revision', '', 0),
(76, 1, '2019-09-23 13:36:02', '2019-09-23 13:36:02', '', 'Our Latest Projects', '', 'inherit', 'closed', 'closed', '', '73-revision-v1', '', '', '2019-09-23 13:36:02', '2019-09-23 13:36:02', '', 73, 'http://localhost/profile.local/2019/09/23/73-revision-v1/', 0, 'revision', '', 0),
(77, 1, '2019-09-23 13:36:02', '2019-09-23 13:36:02', '', 'Our Latest Projects', '', 'inherit', 'closed', 'closed', '', '74-revision-v1', '', '', '2019-09-23 13:36:02', '2019-09-23 13:36:02', '', 74, 'http://localhost/profile.local/2019/09/23/74-revision-v1/', 0, 'revision', '', 0),
(78, 1, '2019-09-23 13:36:44', '0000-00-00 00:00:00', '{\n    "shark-business::header_textcolor": {\n        "value": "#ffffff",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:36:44"\n    },\n    "shark-business::header_image": {\n        "value": "http://localhost/profile.local/wp-content/uploads/2019/09/cropped-slide2-2.jpg",\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:36:44"\n    },\n    "shark-business::header_image_data": {\n        "value": {\n            "attachment_id": 58,\n            "url": "http://localhost/profile.local/wp-content/uploads/2019/09/cropped-slide2-2.jpg",\n            "thumbnail_url": "http://localhost/profile.local/wp-content/uploads/2019/09/cropped-slide2-2.jpg",\n            "alt_text": "",\n            "attachment_parent": 31,\n            "width": 1920,\n            "height": 800,\n            "timestamp": [\n                "1569244015"\n            ]\n        },\n        "type": "theme_mod",\n        "user_id": 1,\n        "date_modified_gmt": "2019-09-23 13:36:44"\n    }\n}', '', '', 'auto-draft', 'closed', 'closed', '', '880e0c89-829f-464c-9b4b-c221006371a7', '', '', '2019-09-23 13:36:44', '0000-00-00 00:00:00', '', 0, 'http://localhost/profile.local/?p=78', 0, 'customize_changeset', '', 0),
(79, 1, '2019-09-27 06:16:43', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-09-27 06:16:43', '0000-00-00 00:00:00', '', 0, 'http://localhost/profile.local/?p=79', 0, 'post', '', 0),
(80, 1, '2019-09-27 06:18:24', '2019-09-27 06:18:24', '', 'image00001', '', 'inherit', 'open', 'closed', '', 'image00001', '', '', '2019-09-27 06:18:24', '2019-09-27 06:18:24', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/image00001.jpeg', 0, 'attachment', 'image/jpeg', 0),
(81, 1, '2019-09-27 06:18:25', '2019-09-27 06:18:25', '', 'image00002', '', 'inherit', 'open', 'closed', '', 'image00002', '', '', '2019-09-27 06:18:25', '2019-09-27 06:18:25', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/image00002.jpeg', 0, 'attachment', 'image/jpeg', 0),
(82, 1, '2019-09-27 06:18:27', '2019-09-27 06:18:27', '', 'image00003', '', 'inherit', 'open', 'closed', '', 'image00003', '', '', '2019-09-27 06:18:27', '2019-09-27 06:18:27', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/image00003.jpeg', 0, 'attachment', 'image/jpeg', 0),
(83, 1, '2019-09-27 06:18:28', '2019-09-27 06:18:28', '', 'image00004', '', 'inherit', 'open', 'closed', '', 'image00004', '', '', '2019-09-27 06:18:28', '2019-09-27 06:18:28', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/image00004.jpeg', 0, 'attachment', 'image/jpeg', 0),
(84, 1, '2019-09-27 06:18:29', '2019-09-27 06:18:29', '', 'image00005', '', 'inherit', 'open', 'closed', '', 'image00005', '', '', '2019-09-27 06:18:29', '2019-09-27 06:18:29', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/image00005.jpeg', 0, 'attachment', 'image/jpeg', 0),
(85, 1, '2019-09-27 06:18:31', '2019-09-27 06:18:31', '', 'image00006', '', 'inherit', 'open', 'closed', '', 'image00006', '', '', '2019-09-27 06:18:31', '2019-09-27 06:18:31', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/image00006.jpeg', 0, 'attachment', 'image/jpeg', 0),
(86, 1, '2019-09-27 06:18:32', '2019-09-27 06:18:32', '', 'image00007', '', 'inherit', 'open', 'closed', '', 'image00007', '', '', '2019-09-27 06:18:32', '2019-09-27 06:18:32', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/image00007.jpeg', 0, 'attachment', 'image/jpeg', 0),
(87, 1, '2019-09-27 06:18:34', '2019-09-27 06:18:34', '', 'image00008', '', 'inherit', 'open', 'closed', '', 'image00008', '', '', '2019-09-27 06:18:34', '2019-09-27 06:18:34', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/image00008.jpeg', 0, 'attachment', 'image/jpeg', 0),
(88, 1, '2019-09-27 06:18:35', '2019-09-27 06:18:35', '', 'image00009', '', 'inherit', 'open', 'closed', '', 'image00009', '', '', '2019-09-27 06:18:35', '2019-09-27 06:18:35', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/image00009.jpeg', 0, 'attachment', 'image/jpeg', 0),
(89, 1, '2019-09-27 06:18:37', '2019-09-27 06:18:37', '', 'image00010', '', 'inherit', 'open', 'closed', '', 'image00010', '', '', '2019-09-27 06:18:37', '2019-09-27 06:18:37', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/image00010.jpeg', 0, 'attachment', 'image/jpeg', 0),
(90, 1, '2019-09-27 06:18:38', '2019-09-27 06:18:38', '', 'image00011', '', 'inherit', 'open', 'closed', '', 'image00011', '', '', '2019-09-27 06:18:38', '2019-09-27 06:18:38', '', 0, 'http://localhost/profile.local/wp-content/uploads/2019/09/image00011.jpeg', 0, 'attachment', 'image/jpeg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE IF NOT EXISTS `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE IF NOT EXISTS `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Main Menu', 'main-menu', 0),
(3, '23', '23', 0),
(4, 'post-format-gallery', 'post-format-gallery', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE IF NOT EXISTS `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(6, 2, 0),
(7, 2, 0),
(8, 2, 0),
(9, 2, 0),
(28, 3, 0),
(29, 3, 0),
(30, 3, 0),
(32, 3, 0),
(35, 3, 0),
(64, 1, 0),
(66, 1, 0),
(69, 1, 0),
(71, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 4),
(2, 2, 'nav_menu', '', 0, 4),
(3, 3, 'ml-slider', '', 0, 4),
(4, 4, 'post_format', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE IF NOT EXISTS `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'fundisa'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:"f902ca0f1871ad29740481d53e8120afe5467c6e307871eb0acb31950964fb78";a:4:{s:10:"expiration";i:1569737800;s:2:"ip";s:3:"::1";s:2:"ua";s:114:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36";s:5:"login";i:1569565000;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '79'),
(18, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:15:"title-attribute";i:2;s:11:"css-classes";i:3;s:3:"xfn";i:4;s:11:"description";}'),
(19, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:"add-post_tag";}'),
(20, 1, 'wp_metaslider_user_saw_callout_toolbar', '1'),
(21, 1, 'nav_menu_recently_edited', '2'),
(22, 1, 'wp_user-settings', 'libraryContent=browse'),
(23, 1, 'wp_user-settings-time', '1569240159'),
(24, 1, 'enable_custom_fields', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE IF NOT EXISTS `wp_users` (
  `ID` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'fundisa', '$P$Bkk3/OCJ1cxclO2CaRunmJoJcUGyOP/', 'fundisa', 'sbuja.gbza@gmail.com', '', '2019-09-17 06:10:00', '', 0, 'fundisa');

-- --------------------------------------------------------

--
-- Table structure for table `wp_yoast_seo_links`
--

CREATE TABLE IF NOT EXISTS `wp_yoast_seo_links` (
  `id` bigint(20) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL,
  `target_post_id` bigint(20) unsigned NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_yoast_seo_links`
--

INSERT INTO `wp_yoast_seo_links` (`id`, `url`, `post_id`, `target_post_id`, `type`) VALUES
(81, 'http://localhost/profile.local/2019/09/23/who-is-thought-to-have/', 12, 69, 'internal'),
(82, 'http://localhost/profile.local/2019/09/23/who-is-thought-to-have/', 12, 69, 'internal'),
(83, 'http://localhost/profile.local/2019/09/23/ipsum-dolor/', 12, 66, 'internal'),
(84, 'http://localhost/profile.local/2019/09/23/ipsum-dolor/', 12, 66, 'internal'),
(85, 'http://localhost/profile.local/2019/09/23/lorem-ipsum/', 12, 64, 'internal'),
(86, 'http://localhost/profile.local/2019/09/23/lorem-ipsum/', 12, 64, 'internal'),
(87, 'http://localhost/profile.local/2019/09/17/hello-world/', 12, 1, 'internal'),
(88, 'http://localhost/profile.local/2019/09/17/hello-world/', 12, 1, 'internal'),
(89, 'http://localhost/profile.local/2019/09/23/who-is-thought-to-have/', 12, 69, 'internal'),
(90, 'http://localhost/profile.local/2019/09/23/who-is-thought-to-have/', 12, 69, 'internal'),
(91, 'http://localhost/profile.local/2019/09/23/ipsum-dolor/', 12, 66, 'internal'),
(92, 'http://localhost/profile.local/2019/09/23/ipsum-dolor/', 12, 66, 'internal'),
(93, 'http://localhost/profile.local/2019/09/23/lorem-ipsum/', 12, 64, 'internal'),
(94, 'http://localhost/profile.local/2019/09/23/lorem-ipsum/', 12, 64, 'internal'),
(95, 'http://localhost/profile.local/2019/09/17/hello-world/', 12, 1, 'internal'),
(96, 'http://localhost/profile.local/2019/09/17/hello-world/', 12, 1, 'internal'),
(97, 'http://localhost/profile.local/2019/09/23/who-is-thought-to-have/', 12, 69, 'internal'),
(98, 'http://localhost/profile.local/2019/09/23/who-is-thought-to-have/', 12, 69, 'internal'),
(99, 'http://localhost/profile.local/2019/09/23/ipsum-dolor/', 12, 66, 'internal'),
(100, 'http://localhost/profile.local/2019/09/23/ipsum-dolor/', 12, 66, 'internal'),
(101, 'http://localhost/profile.local/2019/09/23/lorem-ipsum/', 12, 64, 'internal'),
(102, 'http://localhost/profile.local/2019/09/23/lorem-ipsum/', 12, 64, 'internal'),
(103, 'http://localhost/profile.local/2019/09/17/hello-world/', 12, 1, 'internal'),
(104, 'http://localhost/profile.local/2019/09/17/hello-world/', 12, 1, 'internal');

-- --------------------------------------------------------

--
-- Table structure for table `wp_yoast_seo_meta`
--

CREATE TABLE IF NOT EXISTS `wp_yoast_seo_meta` (
  `object_id` bigint(20) unsigned NOT NULL,
  `internal_link_count` int(10) unsigned DEFAULT NULL,
  `incoming_link_count` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_yoast_seo_meta`
--

INSERT INTO `wp_yoast_seo_meta` (`object_id`, `internal_link_count`, `incoming_link_count`) VALUES
(1, 0, 6),
(4, 0, 0),
(10, 0, 0),
(12, 24, 0),
(44, 0, 0),
(48, 0, 0),
(62, 0, 0),
(64, 0, 6),
(66, 0, 6),
(69, 0, 6),
(71, 0, 0),
(73, 0, 0),
(74, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_yoast_seo_links`
--
ALTER TABLE `wp_yoast_seo_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_direction` (`post_id`,`type`);

--
-- Indexes for table `wp_yoast_seo_meta`
--
ALTER TABLE `wp_yoast_seo_meta`
  ADD UNIQUE KEY `object_id` (`object_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=284;
--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=237;
--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_yoast_seo_links`
--
ALTER TABLE `wp_yoast_seo_links`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=105;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
